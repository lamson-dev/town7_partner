import { StyleSheet } from "react-native";

export const mStyles = StyleSheet.create({
    bkgColorMain: {
        // backgroundColor: "#f6f7f9",
        backgroundColor: "#F2F2F2"
    },
    textMainTitle: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#24253D',
        textAlign: 'center',
    },
    mrgBottMainTitle:{
        marginBottom: 25
    },
    viewBtnMain:{
        marginHorizontal: 20,
        marginTop: 30,
        marginBottom: 25
    },
    
    viewTextBelowBtnMain:{
        flexDirection: 'row',
        justifyContent: 'center',
    },
    textBelowBtnMainLeft:{
        textAlign: 'center',
        marginRight: 5
    },
    textBelowBtnMainRight:{
        color: '#24c3f0',
        fontWeight: 'bold'   
    },
    viewBody: {
        marginHorizontal: 5,
        paddingTop: 15,
        flex: 1,
    },
    textHeading: {
        color: '#474747',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});