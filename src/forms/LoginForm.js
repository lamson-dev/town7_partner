import React, { Component } from "react";
import { View, StyleSheet, Text, Keyboard } from "react-native";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { compose } from "redux";

import { loginAction } from "../redux/actions/auth.action";
import TextInputComp from "../components/TextInputComp";
import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import LoaderModal from "../components/LoaderModal";

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }
    onSubmit = async (values) => {
        Keyboard.dismiss();
        this.setState({ loading: true });
        await this.props.loginUserProps(values);
        setTimeout(() => {
            this.setState({ loading: false });
            var loginStatus = this.props.isLoggedIn;
            this.props.loginProps(loginStatus);
        }, 1700);
    };

    renderInput = ({ secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput }, meta: { touched, error } }) => {
        return (
            <View>
                <TextInputComp
                    {...restInput}
                    placeholder={placeholder}
                    maxLength={maxLength}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    onChangeText={onChange} />
                {(touched && error) && <Text>{error}</Text>}
            </View>
        )
    };


    render() {
        const { handleSubmit } = this.props;
        return (
            <View>
                <LoaderModal
                    loading={this.state.loading} />
                <View style={styles.viewInputEmailPass}>
                    {/* username input field */}
                    <Field
                        name={'username'}
                        placeholder={'Username'}
                        component={this.renderInput} />

                    {/* password intput fiels */}
                    <Field
                        name={'password'}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        component={this.renderInput} />
                </View>

                {/* Sign in button */}
                <View style={mStyles.viewBtnMain}>
                    <ButtonMain nameBtn={'LOGIN'} onPress={handleSubmit(this.onSubmit)} />
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    viewInputEmailPass: {},
});

const validate = (values) => {
    const errors = {};

    if (!values.username || values.username == "") {
        errors.username = "Username is required"
    }
    if (!values.password) {
        errors.password = "Password is required"
    }
    return errors;
}

const mapStateToProps = (state) => {
    console.log('state reducer response: ', state.authUser);
    return {
        isLoggedIn: state.authUser.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUserProps: (payload) => {
            dispatch(loginAction(payload))
        }
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'logIn',
        validate
    })
)(LoginForm);

