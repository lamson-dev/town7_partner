import React, { Component } from "react";
import { View, StyleSheet, Text, Linking, Keyboard } from "react-native";
import CheckBox from "@react-native-community/checkbox";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { compose } from "redux";

import { registerUserAction } from "../redux/actions/auth.action";
import TextInputComp from "../components/TextInputComp";
import ButtonMain from "../components/ButtonMain";
import LoaderModal from "../components/LoaderModal";
import { mStyles } from "../styles/styles";
import { registerUserService } from "../services/authen.service";

class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }
    onSubmit = async (values) => {
        Keyboard.dismiss();
        if (values.isAcceptedTerm != true) {
            alert('Please accept our term');
        } else {
            this.setState({ loading: true });
            await registerUserService(values).then(response => {
                this.setState({ loading: false });
                this.props.registerProps(true);
            }).catch(error => {
                this.setState({ loading: false });
                this.props.registerProps(false);
            })
            // console.log('register response status: ', this.props.registerUser);
            this.setState({ loading: false });
        }
    };

    renderInput = ({ secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput }, meta: { touched, error } }) => {
        return (
            <View>
                <TextInputComp
                    {...restInput}
                    placeholder={placeholder}
                    maxLength={maxLength}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    onChangeText={onChange} />
                {(touched && error) && <Text>{error}</Text>}
            </View>
        )
    };

    renderCheckBox = ({ input: { value, onChange, ...inputRest }, meta: { touched, errors } }) => {
        return (
            <View>
                <CheckBox
                    {...inputRest}
                    value={value ? true : false}
                    onValueChange={(value) => onChange(value)} />
            </View>
        )
    };

    render() {
        const { handleSubmit, registerUser } = this.props;

        return (
            <View>
                <LoaderModal
                    loading={this.state.loading} />
                <View style={styles.viewInputEmailPass}>
                    {/* First name input field */}
                    <Field
                        name={'fullName'}
                        placeholder={'Full Name'}
                        component={this.renderInput} />

                    {/* Last name input field */}
                    <Field
                        name={'email'}
                        placeholder={'Email'}
                        component={this.renderInput} />

                    {/* Phone input field */}
                    <Field
                        name={'phone'}
                        placeholder={'Phone'}
                        keyboardType={'phone-pad'}
                        component={this.renderInput} />

                    {/* Username input field */}
                    <Field
                        name={'username'}
                        placeholder={'Username'}
                        component={this.renderInput} />

                    {/* password intput fiels */}
                    <Field
                        name={'password'}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        component={this.renderInput} />

                    <View style={styles.viewCheckBox}>

                        {/* accepted term field */}
                        <Field
                            name={'isAcceptedTerm'}
                            component={this.renderCheckBox} />

                        <View style={styles.viewTextTerm}>
                            <Text style={styles.textTerm}>
                                <Text>
                                    I accept the
                                </Text>
                                <Text style={styles.textTermLink} onPress={() => { Linking.openURL("https://www.termsfeed.com/blog/privacy-policies-vs-terms-conditions/") }}>
                                    Term of Use and Privacy Policy
                                </Text>
                            </Text>
                        </View>
                    </View>

                </View>

                {/* Sign in button */}
                <View style={mStyles.viewBtnMain}>
                    <ButtonMain nameBtn={'CREATE NEW ACCOUNT'} onPress={handleSubmit(this.onSubmit)} />
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    viewInputEmailPass: {},
    viewCheckBox: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    viewTextTerm: {
        justifyContent: 'center'
    },
    textTerm: {
        color: 'black',
    },
    textTermLink: {
        color: '#3389EE',
        fontWeight: "bold"
    },
});

const validate = (values) => {
    const errors = {};
    if (!values.fullName) {
        errors.fullName = "Full name is required"
    }

    if (!values.email) {
        errors.email = "Email is required"
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }

    if (!values.phone) {
        errors.phone = "Phone is required"
    }

    if (!values.username) {
        errors.username = "Username is required"
    }

    if (!values.password) {
        errors.password = "Password is required"
    } else if (values.password.length < 8) {
        errors.password = "Password is too short"

    }

    return errors;
}

const mapStateToProps = (state) => {
    return {
        registerUser: state.authUser.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerUserProps: (payload) => {
            dispatch(registerUserAction(payload))
        }
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'logIn',
        validate
    })
)(RegisterForm);

