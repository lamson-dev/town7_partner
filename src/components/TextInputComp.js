import React, { Component } from "react";
import { View, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { Icon } from "react-native-elements";

const propTypes = {
    mapElement: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onChangeText: PropTypes.func,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    maxLength: PropTypes.number,
    keyboardType: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    label: PropTypes.string
};

const defaultProps = {
    mapElement: (n) => { },
    onSubmitEditing: () => { },
    onChangeText: () => { },
    value: "",
    placeholder: "",
    maxLength: 200,
    keyboardType: "default",
    secureTextEntry: false,
    label: ""
};

class TextInputComp extends Component {

    state = {
        value: null,
        showPassword: false
    };

    onChangeText = (value) => {
        this.setState({
            value
        }, () => {
            this.props.onChangeText(value);
        })
    }

    hidePassword = () => {
        this.setState({
            value: this.props.value,
            showPassword: !this.state.showPassword
        })
    }

    componentDidMount() {

        this.props.secureTextEntry ? this.setState({ showPassword: false }) : this.setState({ showPassword: true });
    }

    render() {
        const { placeholder, secureTextEntry, keyboardType, maxLength, value, onChangeText, onSubmitEditing, defaultValue, buttonRight, nameIconBtn, typeIconBtn, buttonRightPress } = this.props;


        return (
            <View style={styles.viewInpt}>
                <TextInput style={styles.inpTxt}
                    placeholder={placeholder}
                    secureTextEntry={!this.state.showPassword}
                    keyboardType={keyboardType}
                    maxLength={maxLength}
                    returnKeyType="next"
                    // value={this.state.value}
                    defaultValue={defaultValue}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                />
                {this.props.secureTextEntry ?
                    <View style={[styles.viewEye]}>
                        <TouchableOpacity onPress={this.hidePassword}>
                            <Icon
                                name={this.state.showPassword ? 'eye-with-line' : 'eye'}
                                type='entypo'
                                color="#cccccc"
                            />
                        </TouchableOpacity>
                    </View>
                    : null}

                {this.props.buttonRight ?
                    <View style={[styles.viewEye]}>
                        <TouchableOpacity onPress={buttonRightPress}>
                            <Icon
                                name={nameIconBtn ? nameIconBtn : null}
                                type={typeIconBtn ? typeIconBtn : null}
                                color="#cccccc"
                            />
                        </TouchableOpacity>
                    </View>
                    : null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewInpt: {
        marginVertical: 2.5,
        flexDirection: 'row',
        // marginHorizontal: 16.5,
        height: 46,
        borderRadius: 12.5,
        paddingLeft: 20,
        backgroundColor: '#FFFFFF',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
    },
    inpTxt: {
        width: "85%",
        fontSize: 16
    },
    viewEye: {
        justifyContent: 'center',

    }
});

TextInputComp.defaultProps = defaultProps;

// MyTextInput.propTypes = propTypes;
export default TextInputComp;