import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { Calendar } from 'react-native-calendars';

const nowDate = new Date();
export default class CalendarModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCalendarVisible: props.visibleCalendar,
            currentDate: nowDate.getFullYear() + '-' + ((nowDate.getMonth() + 1) < 10 ? '0' + (nowDate.getMonth() + 1) : (nowDate.getMonth() + 1)) + '-' + nowDate.getDate(),
            dateSeleted: null,
            // dateStartSeleted: null,
            // dateEndSeleted: null,

        }
    }

    // callback
    setDateSeleted = (date) => {
        // console.log('set start is seleted');
        this.setState({
            dateSeleted: date
        })
        this.props.getDateSeletedCallback(date);
    }

    closeModal = () => {
        this.setState({ modalCalendarVisible: false });
    }
    render() {
        return (

            <View style={styles.viewMain}>

                <View style={styles.viewCalendar}>

                    <View style={styles.viewCloseTouch}>
                        <TouchableOpacity style={styles.touchClose} 
                            onPress={this.props.closeCalendar}>
                            <Text style={{fontSize: 27}}>✖</Text>
                        </TouchableOpacity>
                    </View>
                    <Calendar
                        // Initially visible month. Default = Date()
                        // current={this.state.currentDate}
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={this.state.currentDate}
                        // minDate={'2020-04-12'}
                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                        // maxDate={'2012-05-30'}
                        // Handler which gets executed on day press. Default = undefined
                        // onDayPress={(day) => { this.setDateSeleted(this.state.dateStartSeleted == null ? 'start' : 'end', day.dateString) }}
                        onDayPress={(day) => { this.setDateSeleted(day.dateString) }}
                        // Handler which gets executed on day long press. Default = undefinedzbu
                        // onDayLongPress={(day) => { console.log('selected day-long', day) }}
                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                        monthFormat={'yyyy MM'}
                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                        onMonthChange={(month) => { console.log('month changed', month) }}
                        // Hide month navigation arrows. Default = false
                        hideArrows={false}
                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                        // renderArrow={(direction) => (<Arrow />)}
                        // Do not show days of other months in month page. Default = false
                        hideExtraDays={true}
                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                        // day from another month that is visible in calendar page. Default = false
                        disableMonthChange={true}
                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                        firstDay={1}
                        // Hide day names. Default = false
                        // hideDayNames={true}
                        // Show week numbers to the left. Default = false
                        showWeekNumbers={false}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                        onPressArrowLeft={substractMonth => substractMonth()}
                        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                        onPressArrowRight={addMonth => addMonth()}
                        // Disable left arrow. Default = false
                        disableArrowLeft={false}
                        // Disable right arrow. Default = false
                        disableArrowRight={false}
                        markedDates={{
                            [this.state.dateSeleted ? this.state.dateSeleted : this.state.currentDate]: {
                                startingDay: true,
                                selected: true,
                                disableTouchEvent: true,
                                // color: '#f76e19',
                                color: '#24c3f0'
                            },
                            // [this.state.dateEndSeleted]: {
                            //     endingDay: true,
                            //     selected: true,
                            //     disableTouchEvent: true,
                            //     color: '#24c3f0',
                            // },

                            //  "2020-04-15" :{disabled: true, startingDay: true, color: 'green', endingDay: true}
                        }}
                        markingType={'period'}
                        theme={{
                            monthTextColor: '#24c3f0',
                        }}
                    />
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        flexWrap: "nowrap",
        justifyContent: 'center',
        // opacity: 0,
        // backgroundColor: 'green'
        backgroundColor: 'rgba(0,0,0,.65)',
    },
    viewCalendar: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        borderRadius: 15
    },
    viewCloseTouch: {
        // backgroundColor: '#383bff',
        width: 30,
        height: 30,
        marginRight: 10,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        borderWidth: 0
    },
    toucClose:{
        flex: 1,
        justifyContent: 'center',
    }
});