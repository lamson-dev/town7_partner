import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Icon } from "react-native-elements";

export default class CardBookingList extends Component {
    render() {

        const { hostName, hostId, userFullname, userImg, bookingStatus, cancelStatus, bookingDate, price, startDate, endDate, numOfGuest, onPress } = this.props;

        return (
            <View style={styles.viewMain}>
                {/* tbrip card */}
                <TouchableOpacity style={styles.touchCardTrip} onPress={onPress}>
                    <View style={styles.viewTripBooked}>


                        {/* body */}
                        <View style={[styles.viewBodyTripCard, bookingStatus ? null : styles.viewBookingReaded]}>

                            <Image style={styles.imageRoom} width={35} height={35} source={userImg ? { uri: userImg } : require('../assets/images/icons/user-ic-2.png')} />
                            {/* info */}
                            <View style={styles.viewBodyInfo}>
                                {/* short info */}

                                <View style={{ flexDirection: 'row' }}>

                                    {!bookingStatus ?
                                        <Text style={[styles.textSecondColor, styles.textSmall, { width: 150, color: 'red' }]}
                                            numberOfLines={1} ellipsizeMode='tail' >
                                            Booking request: {bookingStatus}
                                        </Text>
                                        :
                                        <Text style={[styles.textSecondColor, styles.textSmall, { width: 150, fontSize: 12.5 }]}
                                            numberOfLines={1} ellipsizeMode='tail' >
                                            Booking request: {bookingStatus}
                                        </Text>
                                    }

                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 150, textAlign: 'right' }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        {bookingDate}
                                    </Text>

                                </View>

                                <Text style={[styles.textBlackColor, styles.textHostTitle, { width: 150 }]}
                                    numberOfLines={2} ellipsizeMode='tail'>
                                    {userFullname}
                                </Text>

                                <View style={styles.viewAddress}>
                                    <Icon
                                        name='date-range'
                                        type='MaterialIcons'
                                        color="#1EA896"
                                        size={14} />
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 180 }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        {startDate} - {endDate}
                                    </Text>

                                </View>

                                <View style={styles.viewAddress}>
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 200 }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        Prop ID: {hostId} - {hostName}
                                    </Text>

                                </View>

                                <View style={styles.viewPrice}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Icon
                                            name="people"
                                            type="MaterialIcons"
                                            color="#9ea0a4"
                                            size={14}
                                        />
                                        <Text style={[styles.textSecondColor, styles.textSmall]}>
                                            {numOfGuest} people
                                        </Text>
                                    </View>
                                    <Text style={[styles.textSecondColor, styles.textSmall, styles.textPrice]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        VND {price}
                                    </Text>
                                </View>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
    },
    touchCardTrip: {
        // marginTop: 5,
        marginBottom: 5 / 2,
    },
    imageRoom: {
        borderRadius: 17.5
    },
    viewTripBooked: {


        borderBottomWidth: 1,
        borderBottomColor: '#c9c9c9',
        borderRadius: 5,

        // shadowColor: "rgba(0,0,0,0.75)",
        // shadowOffset: {
        //     width: 0,
        //     height: 5
        // },
        // shadowOpacity: 0.29,
        // shadowRadius: 3.84,
        // elevation: 3,

        backgroundColor: '#fff',
        width: '100%'
    },
    viewHeadTripBooked: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10,
    },
    textSecondColor: {
        color: '#535355',
    },
    textBlackColor: {
        color: '#302f33',
    },
    viewBodyTripCard: {
        flexDirection: 'row',
        paddingLeft: 5,
        paddingVertical: 5,
    },
    viewBookingReaded: {
        backgroundColor: '#cfcfcf',
    },
    viewBodyInfo: {
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        flex: 1,
    },
    viewAddress: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    textHostTitle: {
        fontSize: 15,
    },
    textSmall: {
        fontSize: 11,
    },
    textStar: {
        fontSize: 13.5,
        color: '#1EA896',
    },
    viewPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textPrice: {
        width: 200,
        fontWeight: "700",
        color: '#ff8519',
        textAlign: 'right',
        alignSelf: 'flex-end'
    }
});