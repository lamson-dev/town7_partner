import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";

export default class CardNotification extends Component {
    render() {
        const { onPress, title, content, time, status } = this.props;
        return (
            <View>

                {/* Notification card */}
                <TouchableOpacity style={styles.touchNotification} onPress={onPress}>
                    <View style={styles.viewWrapNotification}>

                        {/* body */}
                        <View style={[styles.viewBodyNotification, !status ? styles.viewNotificationReaded : null]}>

                            {/* <Image style={styles.imageNotification} width={35} height={35} source={userImg ? { uri: userImg } : require('../assets/images/icons/user-ic-2.png')} /> */}
                            <Icon
                                name='notification'
                                type='antdesign'
                                color="#1EA896"
                                size={30} />

                            {/* info */}
                            <View style={styles.viewBodyInfo}>
                                {/* short info */}

                                <Text style={[styles.textBlackColor, styles.textNotificationTitle,]}
                                    numberOfLines={2} ellipsizeMode='tail'>
                                    {title}
                                </Text>

                                <View style={styles.viewContentNotification}>

                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 180 }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        {content}
                                    </Text>

                                </View>

                                <View style={styles.viewTime}>

                                    <View style={{ flexDirection: 'row' }}>

                                        <Text style={[styles.textSecondColor, styles.textSmall]}>
                                            {time}
                                        </Text>
                                    </View>

                                </View>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    touchNotification: {
        // marginTop: 5,
        marginBottom: 5 / 2,
    },
    imageNotification: {
        borderRadius: 17.5
    },
    viewWrapNotification: {


        borderBottomWidth: 1,
        borderBottomColor: '#c9c9c9',
        borderRadius: 5,

        backgroundColor: '#fff',
        width: '100%'
    },
    textSecondColor: {
        color: '#535355',
    },
    textBlackColor: {
        color: '#302f33',
    },
    viewBodyNotification: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5,
        paddingVertical: 5,
    },
    viewNotificationReaded: {
        backgroundColor: '#dedede',
    },
    viewBodyInfo: {
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        flex: 1,
    },
    viewContentNotification: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    textNotificationTitle: {
        fontSize: 15,
    },
    textSmall: {
        fontSize: 11,
    },
    viewTime: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textTime: {
        width: 200,
        fontWeight: "700",
        color: '#ff8519',
        textAlign: 'right',
        alignSelf: 'flex-end'
    }
});