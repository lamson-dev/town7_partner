import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default class FabButton extends Component {

    render() {
        const { onPress, title } = this.props;
        return (
            <TouchableOpacity style={styles.touchFab} onPress={onPress}>
                <View>
                    <Text style={styles.textTitle}>
                        {title}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    touchFab: {
        position: 'absolute',
        bottom: 30,
        right: 15,
        backgroundColor: '#24c3f0',
        width: 100,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7.5,
        zIndex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 4,
    },
    textTitle:{
        color:'#fff',
        fontWeight: "bold"
    }
});