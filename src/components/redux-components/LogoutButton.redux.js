import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { connect } from "react-redux";
import { logoutAction } from "../../redux/actions/auth.action";

class LogoutButton extends Component {
    logout = () => {
        this.props.logoutProps();
    }
    render() {
        const { isLoggedIn } = this.props;
        if (isLoggedIn == false) {
            console.log('logged out tam biet: ', isLoggedIn);
            this.props.checkLogout(isLoggedIn);
        };
        return (
            < View style={styles.viewLogout} >
                <TouchableOpacity style={styles.touchLogout} onPress={this.logout}>
                    <Text style={styles.textTouchLogout}>Log Out</Text>
                </TouchableOpacity>
            </View >
        )
    }
}

const styles = StyleSheet.create({

    viewLogout: {
        marginBottom: 4.5
    },
    touchLogout: {
        backgroundColor: '#ffff',
        marginHorizontal: 27,
        paddingVertical: 9,
        borderRadius: 25,
        alignItems: 'center',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    textTouchLogout: {
        fontSize: 16,
        fontWeight: '500',
        color: '#213B73'
    }
});

const mapStateToProps = (state) => {
    console.log('logout map state: ', state.authUser.isLoggedIn);
    return {
        isLoggedIn: state.authUser.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        logoutProps: () => {
            dispatch(logoutAction())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton)