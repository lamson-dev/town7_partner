import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default class ButtonMain extends Component {
    render() {
        return (
            <View style={styles.viewSignInBtn}>
                <TouchableOpacity style={styles.touchSignIn}
                    onPress={this.props.onPress}>
                    <Text style={styles.textSignInTouch}>
                        {this.props.nameBtn}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewSignInBtn: {
        height: 56,
    },
    touchSignIn: {
        flex: 1,
        backgroundColor: '#24c3f0',
        justifyContent: 'center',
        borderRadius: 26.5,

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 5,
    },
    textSignInTouch: {
        color: '#FFFFFF',
        fontSize: 21.2,
        fontWeight: "normal",
        fontStyle: 'normal',
        textAlign: 'center'
    },
});