import { Alert } from 'react-native';

const showAlert = (title, content) => {
    Alert.alert(
        title,
        content,
        [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
        ]
    );
}

export const showAlertFeatureIncomplete = (title, content) => {
    showAlert('Notification', 'This feature is incomplete \nPlease try later');
}