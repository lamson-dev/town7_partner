import { AsyncStorage, YellowBox } from "react-native";
YellowBox.ignoreWarnings(["Warning: AsyncStorage has been extracted from react-native core and will be removed in a future release. It can now be installed and imported from '@react-native-community/async-storage' instead of 'react-native'. See https://github.com/react-native-community/react-native-async-storage"]);

export async function getToken() {

    let accessToken = await AsyncStorage.getItem('token');

    if (accessToken) {
        return accessToken
    } else {
        return null;
    }
}

export async function removeToken() {
    try {
        await AsyncStorage.removeItem('token');
        console.log('Token was removed');
    } catch (error) {
        console.log('Remove token error: ', error);
    }
}