import { fetchApi } from "./api/api";

class HostCityService {

    async getHostCities() {
        return await fetchApi("/host-cities", "GET",null);
    }
}

export default new HostCityService();
