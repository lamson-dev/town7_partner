import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";
import messaging from "@react-native-firebase/messaging";

export const registerUserService = async (requestData) => {
    requestData['deviceToken'] = await messaging().getToken();
    return fetchApi("/users/sign-up", "POST", requestData);
}

export const loginUserService = async (requestData) => {
    requestData['deviceToken'] = await messaging().getToken();
    return fetchApi("/users/sign-in", "POST", requestData);
}

export const logoutUserService = async () => {
    const requestData = {
        'deviceToken': await messaging().getToken()
    };
    const authToken = await deviceStorage.loadToken();
    return fetchApi("/users/sign-out", "POST", requestData, authToken);
}