import { fetchApi } from "./api/api";

class HostCategoryService {

    async getHostCategories() {
        return await fetchApi("/admin/host-categories", "GET",null);
    }
}

export default new HostCategoryService();
