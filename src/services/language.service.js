import { fetchApi } from "./api/api";

class LanguageService {

    async getLanguages() {
        return await fetchApi("/admin/languages", "GET",null);
    }
}

export default new LanguageService();
