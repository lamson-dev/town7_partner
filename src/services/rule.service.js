import { fetchApi } from "./api/api";

class RuleService {

    async getRules() {
        return await fetchApi("/admin/rules", "GET",null);
    }
}

export default new RuleService();
