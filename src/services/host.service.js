import { fetchApi } from "./api/api";
import { getToken } from "./auth-header.service";

class HostService {
    async createHost(data) {
        return fetchApi("/hosts", "POST", data, await getToken());
    }
    async getHost() {
        return fetchApi("/users/hosts", "GET", null, await getToken());
    }
}

export default new HostService();
