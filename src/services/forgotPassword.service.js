import { fetchApi } from "./api/api";

export async function sendCodeViaEmail(email) {
    return fetchApi("/users/send-code-via_email", "POST", email);
}

export async function updatePasswordWithCode(newPassword_Code) {
    return fetchApi("/users/update-password-with-code", "PUT", newPassword_Code);
}