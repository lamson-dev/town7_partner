import { fetchApi } from "./api/api";
import { getToken } from "./auth-header.service";

class BookingRoomService {
    async confirmBooking(id) {
        return fetchApi("/bookings-confirm-request/" + id, "PUT", null, await getToken());
    }
    async cancelBooking(id) {
        return fetchApi("/bookings-cancel/" + id, "PUT", null, await getToken());
    }
    async getAll() {
        return fetchApi("/bookings/users", "GET", null, await getToken());
    }
    async getById(id) {
        return fetchApi("/bookings/" + id, "GET", null, await getToken());
    }
    async getBookingCalendar() {
        return fetchApi("/bookings/users/calendar", "GET", null, await getToken());
    }
    async getBookingAnalysing() {
        return fetchApi("/bookings/analysing", "GET", null, await getToken());
    }
}

export default new BookingRoomService();
