import { REGISTER_USER_SUCCESS, REGISTER_USER_ERROR, AUTH_USER_SUCCESS, AUTH_USER_ERROR, LOGOUT_SUCCESS } from "../actions/types";

const initalState = []

export const registerUser = (state = initalState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            console.log('register success reducer: ', action.response);
            return {
                ...state,
                action,
                isSuccess: true,
                error: null
            };

        case REGISTER_USER_ERROR:
            console.log('register fail reducer');
            return {
                ...state,
                action,
                isSuccess: false,
                error: 'Please try again with other information'
            };


        default:
            return state;
    }
}

export const authUser = (state = initalState, action) => {
    switch (action.type) {
         case AUTH_USER_SUCCESS:
            console.log('-lg reducer login token: ', action);
            return {
                ...state,
                token: action.token,
                userId: action.userId,
                isLoggedIn: true
            };

        case AUTH_USER_ERROR:
            console.log('-lg reducer login error');
            return {
                ...state,
                token: null,
                userId: null,
                isLoggedIn: false
            }
        case LOGOUT_SUCCESS:
            console.log('logout reducer');
            return {
                ...state,
                token: null,
                userId: null,
                isLoggedIn: false
            }
        default:
            return state;
    }
}