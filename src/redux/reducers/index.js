import { combineReducers } from "redux";
import { authUser, registerUser } from "./auth.reducer";
import { reducer as formReducer } from "redux-form";

const reducers = {
    authUser,
    registerUser,
    form: formReducer,
}

const rootReducer = combineReducers(reducers);

export default rootReducer;
