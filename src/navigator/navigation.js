import { Navigation } from "react-native-navigation";

const selectedIconColor = '#0089da';

export const goToLogin = () => Navigation.setRoot({
    root: {

        stack: {
            id: "stackMain",
            children: [
                {

                    component: {
                        name: "LoginScreen",
                        options: {
                            topBar: {
                                visible: false
                            },
                            // statusBar:{
                            //   backgroundColor: 'red',
                            //   style: "dark"
                            // }
                        }
                    },

                }
            ]
        }
    }
});

export const showSideTabsMenu = (userData) => {

    Navigation.setRoot({
        root: {

            stack: {
                children: [
                    {
                        component: {
                            name: 'HomeScreen',
                            options: {
                                topBar:{
                                    visible: false,
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                },
                            },
                        },

                    }
                ]

            }
        }
    });
}
