import { Navigation } from "react-native-navigation";

// import IntroScreen from "../screens/IntroScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import SuccessfulRegisterScreen from "../screens/SuccessfulRegisterScreen";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import VerificationCodeScreen from "../screens/forgot-password/VerificationCodeScreen";
import ResetPasswordScreen from "../screens/forgot-password/ResetPasswordScreen";
import SuccessfulResetPasswordScreen from "../screens/forgot-password/SuccessfulResetPasswordScreen";
import SuccessfulRoomScreen from "../screens/add-room/SuccessfulRoomScreen";
import HomeScreen from "../screens/HomeScreen";
import ListPropertiesScreen from "../screens/ListPropertiesScreen";
import BookingManagementScreen from "../screens/BookingManagementScreen";
import AnalytiscScreen from "../screens/AnalytiscScreen";
import NotificationScreen from "../screens/NotificationScreen";
import BookingDetailScreen from "../screens/BookingDetailScreen";
import BookingCalendarScreen from "../screens/BookingCalendarScreen";
import UserProfileScreen from "../screens/UserProfileScreen";

export function registerScreen() {
  Navigation.registerComponent('LoginScreen', () => LoginScreen);
  Navigation.registerComponent('RegisterScreen', () => RegisterScreen);
  Navigation.registerComponent('SuccessfulRegisterScreen', () => SuccessfulRegisterScreen);
  Navigation.registerComponent('ForgotPasswordScreen', () => ForgotPasswordScreen);
  Navigation.registerComponent('VerificationCodeScreen', () => VerificationCodeScreen);
  Navigation.registerComponent('ResetPasswordScreen', () => ResetPasswordScreen);
  Navigation.registerComponent('SuccessfulResetPasswordScreen', () => SuccessfulResetPasswordScreen);
  Navigation.registerComponent('HomeScreen', () => HomeScreen);
  Navigation.registerComponent('ListPropertiesScreen', () => ListPropertiesScreen);
  Navigation.registerComponent('BookingManagementScreen', () => BookingManagementScreen);
  Navigation.registerComponent('BookingCalendarScreen', () => BookingCalendarScreen);
  Navigation.registerComponent('NotificationScreen', () => NotificationScreen);
  Navigation.registerComponent('AnalytiscScreen', () => AnalytiscScreen);
  Navigation.registerComponent('UserProfileScreen', () => UserProfileScreen);
  Navigation.registerComponent('SuccessfulRoomScreen', () => SuccessfulRoomScreen);
  Navigation.registerComponent('BookingDetailScreen', () => BookingDetailScreen);
}