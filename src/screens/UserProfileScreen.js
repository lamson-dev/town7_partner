import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, } from "react-native";
import { Icon } from "react-native-elements";
import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";

import store from "../redux/store/store";
import { mStyles } from "../styles/styles";
import LogoutButton from "../components/redux-components/LogoutButton.redux";
import { goToLogin } from '../navigator/navigation';

export default class UserProfileScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName: props.fullName,
            email: props.email,
            phone: props.phone,
            loading: false
        };

    }

    logout = async (isLoggedIn) => {
        console.log('logged out UserScr: ', isLoggedIn);
        goToLogin();
    }
    componentDidMount() {
    }
    render() {
        return (
            <View style={[styles.viewMain]}>

                <ScrollView>

                    {/* top view */}
                    <View style={styles.viewTop}>
                        <View style={styles.viewUserInfo}>
                            <View style={styles.viewAvt}>
                                <Image style={styles.imageAvt} source={{ uri: 'https://www.pngkit.com/png/detail/128-1280585_user-icon-fa-fa-user-circle.png' }} />
                            </View>

                            <View style={styles.viewTopRight}>
                                <Text style={styles.textName}>
                                    {this.state.fullName}
                                </Text>
                            </View>
                        </View>
                    </View>

                    {/* options view */}
                    <View style={styles.viewOption}>

                        <View style={styles.viewSubOption}>
                            <View style={styles.viewTouchOption}>

                                <View style={[styles.viewIconOption]}>
                                    <Icon
                                        name='user'
                                        type='antdesign'
                                        color="#0089da"
                                    />
                                </View>


                                <Text style={styles.textTouchOption}>
                                    {this.state.fullName}
                                </Text>

                            </View>
                        </View>

                        <View style={styles.viewSubOption}>
                            <View style={styles.viewTouchOption}>

                                <View style={[styles.viewIconOption]}>
                                    <Icon
                                        name='email'
                                        type='Fontisto'
                                        color="#0089da"
                                    />
                                </View>


                                <Text style={styles.textTouchOption}>
                                    {this.state.email}
                                </Text>

                            </View>
                        </View>

                        <View style={styles.viewSubOption}>
                            <View style={styles.viewTouchOption}>

                                <View style={[styles.viewIconOption]}>
                                    <Icon
                                        name='phone'
                                        type='AntDesign'
                                        color="#0089da"
                                    />
                                </View>


                                <Text style={styles.textTouchOption}>
                                    {this.state.phone}
                                </Text>

                            </View>
                        </View>

                    </View>

                    {/* Log out Touch */}
                    <Provider store={store}>
                        <LogoutButton
                            checkLogout={this.logout}
                        />
                    </Provider>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        // justifyContent: 'center'
        backgroundColor: '#ffff',
    },
    viewTop: {
        // backgroundColor: 'blue',
        // flexDirection: 'row',
        paddingHorizontal: 12.5,
        paddingTop: 15,
        paddingBottom: 5,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 5,
    },
    viewAvt: {
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 80,
        width: 80,
        height: 80,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 5.49,

        elevation: 2,
    },
    imageAvt: {
        width: "100%",
        height: "100%",
        borderRadius: 80,
        borderWidth: 2,

    },
    viewTopRight: {
        // marginLeft: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textName: {
        // marginTop: 15,
        fontWeight: 'bold',
        fontSize: 22.5

    },
    touchEdit: {
        marginVertical: 5,
        borderRadius: 11.5,
        borderWidth: 1,
        borderColor: '#3389EE',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    textEdit: {
        color: '#3389EE',
        fontWeight: "bold"
    },

    // style for view option
    viewOption: {
        marginVertical: 15,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: '#ffff',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 5,
    },
    viewSubOption: {
        paddingVertical: 7.5,
        marginHorizontal: 15,
        borderBottomWidth: 1.5,
        borderBottomColor: '#f6f7f9'
    },
    touchOption: {

    },
    viewTouchOption: {
        flexDirection: 'row'
    },
    viewIconOption: {
        width: 43.5,
        height: 43.5,
        borderRadius: 35,
        justifyContent: 'center',
    },
    iconOption: {

    },
    textTouchOption: {
        alignSelf: 'center',
        paddingLeft: 15,
        fontSize: 17.5,
        fontWeight: "800"
    },
    viewRightIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        right: 15
    },
    iconRight: {
    },

    viewLogout: {
        marginBottom: 4.5
    },
    touchLogout: {
        backgroundColor: '#ffff',
        marginHorizontal: 27,
        paddingVertical: 9,
        borderRadius: 25,
        alignItems: 'center',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    textTouchLogout: {
        fontSize: 16,
        fontWeight: '500',
        color: '#213B73'
    },

    viewUserInfo: {
        backgroundColor: '#fff',
        borderRadius: 5,
        paddingVertical: 5,
        paddingHorizontal: 8.5,

    },
    viewField: {
        borderBottomWidth: 1.5,
        borderBottomColor: '#f6f7f9',
        paddingBottom: 10
    },
    textLabelField: {
        fontSize: 14,
        color: 'grey'
    },
    textField: {
        fontSize: 17.5,
        fontWeight: "700"
    }
});