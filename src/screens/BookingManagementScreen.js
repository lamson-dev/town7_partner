import React, { Component } from "react";
import { View, StyleSheet, FlatList } from 'react-native';
import { Navigation } from "react-native-navigation";

import CardBookingList from "../components/CardBookingList";
import LoaderModal from "../components/LoaderModal";

import BookingRoomService from "../services/bookingRoom.service";
export default class BookingManagementScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingData: null,
            loading: false
        }
    }

    goToBookingDetail = (id = 0) => {

        Navigation.push(this.props.componentId, {
            component: {
                name: 'BookingDetailScreen',
                passProps: {
                    id: id
                },
                options: {
                    topBar: {
                        visible: true,
                        title: {
                            text: 'Booking Detail',
                        }
                    },
                    statusBar: {
                        backgroundColor: '#1ab3c7',
                        style: 'light'
                    }
                }
            }
        });
    }

    async componentDidMount() {
        this.navigationEventListener = Navigation.events().bindComponent(this);

        this.setState({ loading: true });
        await BookingRoomService.getAll().then((response) => {
            console.log(response);
            this.setState({ loading: false });
            this.setState({ bookingData: response.data.data });
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error, can not load your booking: ' + error);
        });
    }
    
    async componentDidAppear() {
        this.setState({ loading: true });
        await BookingRoomService.getAll().then((response) => {
            console.log(response);
            this.setState({ loading: false });
            this.setState({ bookingData: response.data.data });
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error, can not load your booking: ' + error);
        });
    }

    renderCardBookingRoom = (bookings) => {
        return (
            <FlatList
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={bookings}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardBookingList
                            hostName={item.host.name}
                            hostId={item.host.id}
                            userFullname={item.user.fullName}
                            userImg=""
                            // bookingDate={new Date(item.createdAt).getFullYear()+"-"+(new Date(item.createdAt).getMonth()+1)+"-"+new Date(item.createdAt).getDate()}
                            bookingDate={item.createDate}
                            price={item.totalPrice}
                            startDate={item.checkInDate}
                            endDate={item.checkOutDate}
                            numOfGuest={item.numberOfAdultGuest +"-"+ item.numberOfChildrenGuest +"-"+ item.numberOfInfantGuest}
                            onPress={() => this.goToBookingDetail(item.id)}
                            bookingStatus={item.acceptedFromHost} 
                            cancelStatus={true}
                            />
                }
            />
        )
    }

    render() {
        return (
            <View style={styles.viewMain}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* tbrip card */}
                {this.renderCardBookingRoom(this.state.bookingData)}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    }

});