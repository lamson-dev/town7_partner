import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";
import { Provider } from "react-redux";
import { Navigation } from "react-native-navigation";

import LoginForm from "../forms/LoginForm";
import { mStyles } from "../styles/styles";
import store from "../redux/store/store";
import { showSideTabsMenu } from "../navigator/navigation";

export default class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    goToRegister = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'RegisterScreen'
            }
        });
    }

    goToForgotPassword = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ForgotPasswordScreen',
            }
        })
    }

    login = (loginStatus) => {
        if (loginStatus == false) {
            // this.setState({ loading: true });
            alert('Please check your username or password.');
        }
        if (loginStatus == true) {
            showSideTabsMenu();
        };
    }

    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <ScrollView>

                    {/* <Text>Login Screen</Text> */}
                    <View style={styles.viewLogo}>
                        <Image style={styles.imgLogo} source={require('../assets/images/houses-for-sale-in-png-5.png')}></Image>
                        <Text style={mStyles.textMainTitle}>Sign in</Text>
                    </View>

                    <Provider store={store}>
                        <LoginForm loginProps={this.login} />
                    </Provider>

                    {/* Forgot password */}
                    <View style={styles.viewForgotPass}>
                        <TouchableOpacity style={styles.touchForgotPass} onPress={this.goToForgotPassword}>
                            <Text style={styles.textForgotPassTouch}>
                                Forgot password?
                            </Text>
                        </TouchableOpacity>
                    </View>

                    {/* Sign up */}
                    <View style={styles.viewSignUp}>
                        <Text style={styles.textOther}>
                            Don't have account?
                        </Text>
                        <TouchableOpacity style={styles.touchSignUp} onPress={this.goToRegister}>
                            <Text style={styles.textTouchSignUp}> create a new account</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },

    viewLogo: {
        paddingVertical: 25,
        justifyContent: 'center',
    },
    imgLogo: {
        width: 170,
        height: 100,
        alignSelf: 'center',
    },

    viewInputEmailPass: {},

    viewSignInBtn: {
        marginHorizontal: 20,
    },

    viewForgotPass: {
        justifyContent: 'center',
    },
    touchForgotPass: {
        alignSelf: 'center'
    },
    textForgotPassTouch: {
        fontWeight: '700',
        fontSize: 15,
        lineHeight: 18,
        textAlign: 'center',
        color: "#24c3f0"
    },

    viewSignInOther: {},
    textOther: {
        color: '#000000',
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 15,
        textAlign: 'center',
        marginVertical: 5
    },
    viewSignInSub: {
        height: 46,
        marginHorizontal: 20,
        marginVertical: 5
    },
    touchSignInSub: {
        flex: 1,
        borderRadius: 26.5,
        justifyContent: 'center',
    },
    textTouchSub: {
        color: '#FFFFFF',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 17.2,
        textAlign: 'center'
    },

    viewSignUp: {
        flexDirection: 'row',
        justifyContent: 'center',
        // backgroundColor: '#FE3232',
        // marginVertical: 25
    },
    viewSignUpSub: {
        height: 50,
        backgroundColor: '#FE3232',

    },
    touchSignUp: {
        justifyContent: 'center',
    },
    textTouchSignUp: {
        color: '#24c3f0',
        fontWeight: '700',
        fontSize: 15,
        lineHeight: 18,
    }
});