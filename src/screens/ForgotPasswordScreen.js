import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { Navigation } from "react-native-navigation";

import TextInputComp from "../components/TextInputComp";
import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import { sendCodeViaEmail } from "../services/forgotPassword.service";

export default class ForgotPasswordScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: null
        }
    }

    popLogin = () => {
        Navigation.pop(this.props.componentId);
    }

    goToVerifiCodeScreen = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'VerificationCodeScreen',
            }
        });
    }

    inputEmail = (email) => {
        console.log('email: ', email)
        this.setState({
            email: email,
        });
    }
    showEmail = async () => {
        console.log('email state: ', this.state.email);
        // check validation email
        if (!this.state.email) {
            alert('email empty');
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.state.email)) {
            alert('Invalid email address');
        } else {
            const emailObj = { email: this.state.email };
            console.log(emailObj);
            const dataSendCode = await sendCodeViaEmail(emailObj);
            console.log('respone send code: ', dataSendCode.data);
            if (dataSendCode.data.totalRecord <= 0) {
                alert('Email is not existing');
            } else {
                this.goToVerifiCodeScreen();
            }
        }
    }
    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <ScrollView>
                    <View>
                        <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                            Forgot password?
                    </Text>

                        <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                            <Text style={{ fontWeight: 'bold' }}>
                                Please enter your registered email.
                        </Text>
                            <Text style={{ color: 'grey' }}>
                                We will send a verification code to your registered email
                        </Text>
                        </View>

                        <TextInputComp
                            placeholder={'Email'}
                            onChangeText={(value) => this.inputEmail(value)} />

                        <View style={mStyles.viewBtnMain}>
                            <ButtonMain nameBtn={'Next'} onPress={() => this.showEmail()} />
                        </View>

                        <View style={styles.viewAccountReady}>
                            <Text style={{ textAlign: 'center', marginRight: 5 }}>
                                Back to
                        </Text>
                            <TouchableOpacity onPress={this.popLogin}>
                                <Text style={{ color: '#3389EE', fontWeight: 'bold' }}>
                                    Sign in
                            </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

    viewAccountReady: {
        flexDirection: 'row',
        justifyContent: 'center',
    }
});