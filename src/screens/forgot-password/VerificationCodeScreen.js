import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

import TextInputComp from "../../components/TextInputComp";
import ButtonMain from "../../components/ButtonMain";
import { mStyles } from "../../styles/styles";
import { updatePasswordWithCode } from "../../services/forgotPassword.service";
import { Navigation } from "react-native-navigation";

export default class VerificationCodeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: null,
            newPassword: null,
            confirmNewPassword: null
        }
    }
    goToSuccessfulResetPassword = () => {
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: "SuccessfulResetPasswordScreen",
                            options:{
                                topBar:{
                                    visible: false
                                }
                            }
                        }
                    }
                ]
            }
        })
    }

    inputInfor = async () => {
        const newPassword_Code = {
            code: this.state.code,
            newPassword: this.state.newPassword,
            confirmNewPassword: this.state.confirmNewPassword
        }

        if (newPassword_Code.code) {

            if (newPassword_Code.newPassword != newPassword_Code.confirmNewPassword) {
                alert('New password and confirm password is not match');
            } else {
                console.log(newPassword_Code);
                const dataUpdatePassword = await updatePasswordWithCode(newPassword_Code);
                console.log('-: ', dataUpdatePassword.data);
                if (dataUpdatePassword.data.totalRecord <= 0) {
                    alert('The code you entered is incorrect or expired')
                } else {
                    // alert('Password Reset Successful');
                    this.goToSuccessfulResetPassword();
                }
            }
        } else {
            alert('You need to enter your code')
        }
    }
    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <View>

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Please enter your verification code.
                        </Text>
                        <Text style={{ color: 'grey' }}>
                            We have sent a verification code to your registered email
                        </Text>
                    </View>

                    <TextInputComp
                        placeholder={'Verification code'}
                        keyboardType={'number-pad'}
                        onChangeText={(value) => this.setState({ code: value })} />

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Please enter a new password
                        </Text>
                    </View>

                    <TextInputComp
                        placeholder={'New Password'}
                        secureTextEntry={true}
                        onChangeText={(value) => this.setState({ newPassword: value })} />

                    <TextInputComp
                        placeholder={'Re-enter New Password'}
                        secureTextEntry={true}
                        onChangeText={(value) => this.setState({ confirmNewPassword: value })} />

                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'Done'} onPress={this.inputInfor} />
                        {/* <ButtonMain nameBtn={'Done'} onPress={this.goToSuccessfulResetPassword} /> */}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

    viewAccountReady: {
        flexDirection: 'row',
        justifyContent: 'center',
    }
});