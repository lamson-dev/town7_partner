import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList } from "react-native";
import { Icon } from "react-native-elements";
import { Navigation } from "react-native-navigation";

import RoomDetailScreen from "./RoomDetailScreen";
import RoomTitleScreen from "../screens/add-room/RoomTitleScreen";
import TextInputComp from "../components/TextInputComp";
import LoaderModal from "../components/LoaderModal";
import CardListing from "../components/CardListing";
import HostService from "../services/host.service";

export default class ListPropertiesScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostData: [],
            loading: false
        };
    }

    goToBookingDetail = () => {

        Navigation.registerComponent('RoomDetailScreen', () => RoomDetailScreen);

        Navigation.push(this.props.componentId, {
            component: {
                name: 'RoomDetailScreen',
                passProps: {
                },
                options: {
                    topBar: {
                        visible: false,

                    },
                    statusBar: {
                        backgroundColor: '#1ab3c7',
                        style: 'light'
                    }
                }
            }
        });
    }

    goToRoomTitle = () => {

        Navigation.registerComponent('RoomTitleScreen', () => RoomTitleScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'RoomTitleScreen',
                            passProps: {
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'Title',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        })
    }

    async componentDidMount() {
        this.navigationEventListener = Navigation.events().bindComponent(this);
        this.setState({ loading: true });
        await HostService.getHost().then(hostData => {
            this.setState({ loading: false });
            this.setState({ hostData: hostData.data.data });
        }).catch(error => {
            this.setState({ loading: false });
            console.log('error: ', error)
            alert('Can not load your hosts:'+ error);
        });
    }

    async componentWillUnmount() {
        if (this.navigationEventListene) {
        }
    }

    async componentDidAppear() {
        this.setState({ loading: true });
        await HostService.getHost().then(hostData => {
            this.setState({ loading: false });
            this.setState({ hostData: hostData.data.data });
        }).catch(erro => {
            this.setState({ loading: false });
            alert('Can not load your hosts')
        });
    }

    // Recommended homestays for you
    renderHostCard = (host) => {
        return (
            <FlatList

                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={host}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardListing
                            name={item.name}
                            address={item.address}
                            standardPriceMondayToThursday={item.standardPriceMondayToThursday}
                            numberOfMaximumGuest={item.numberOfMaximumGuest}
                            bedroomCount={item.bedroomCount} />
                }
            />
        )
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* body view */}
                <View style={styles.viewBody}>
                    {/* row menu */}

                    <View style={styles.viewRowMenu}>
                        {/* item menu */}
                        {this.renderHostCard(this.state.hostData)}
                    </View>

                    {/* floating button */}
                    <TouchableOpacity style={styles.touchFab} onPress={this.goToRoomTitle}>
                        <View>
                            <Icon
                                name="add"
                                type="MaterialIcons"
                                color="#ffff"
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },
    viewTop: {
        width: "100%",
        flex: 1,
        // backgroundColor: 'blue',
        justifyContent: 'center'
    },

    viewManipul: {
        flexDirection: 'row',
        // paddingVertical: 5
    },
    viewManipulItem: {
        backgroundColor: '#fff',
        marginRight: 5,
        borderWidth: 1.5,
        borderColor: '#24c3f0',
        borderRadius: 5
    },
    touchManipul: {
        paddingHorizontal: 10,
        paddingVertical: 4,
    },
    textTouchManipul: {
        color: '#24c3f0',
        fontWeight: '700'
    },
    viewSeachList: {
        // paddingVertical: 15,
    },

    viewBody: {
        flex: 4,
    },
    viewRowMenu: {
        marginHorizontal: 2.5

    },

    touchFab: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor: '#24c3f0',
        width: 50,
        height: 50,
        justifyContent: 'center',
        borderRadius: 25,
        zIndex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 4,
    }
});