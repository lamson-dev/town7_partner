import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions } from "react-native";
import { Icon } from "react-native-elements";
import { WebView } from "react-native-webview";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import { showAlertFeatureIncomplete } from "../utils/alert.utils";
import ReviewScreen from "./ReviewScreen";

export default class RoomDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            // roomData: props.roomDataProps
        };
    }

    gotoReview = () => {
        Navigation.registerComponent('ReviewScreen', () => ReviewScreen)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ReviewScreen',
                passProps: {

                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#3389EE',
                        },
                        // drawBehind: true,
                        background: {
                            color: '#F2F2F2'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    renderStar = (numOfStar) => {
        let star = [];

        for (let index = 0; index < 5; index++) {

            if (index < numOfStar) {
                star.push(<Text key={index} style={styles.textStar}>★</Text>);
            } else {
                star.push(<Text key={index} style={styles.textStar}>☆</Text>);
            }
        };
        return star;
    }

    render() {

        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>

                {/* top view: image background, name of room */}
                <View style={{ backgroundColor: '#ffff' }}>
                    {/* Image */}
                    <View style={{ flexDirection: 'row' }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <Image style={styles.imageRoom} source={{ uri: "https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg" }} />
                            <Image style={styles.imageRoom} source={{ uri: "https://media1-reatimes.cdn.vccloud.vn/media/uploaded/41/2018/11/02/homestay-1.jpg" }} />
                        </ScrollView>
                    </View>
                    {/* Wish Button */}
                    <View style={styles.viewWish_Share}>
                        {/* Wish button */}
                        <TouchableOpacity
                            style={[styles.touchWish_Share, { backgroundColor: '#31d46a' }]}
                            onPress={() => showAlertFeatureIncomplete()}>
                            <Icon
                                name="edit"
                                type="feather"
                                color="#fff"
                            />
                        </TouchableOpacity>

                        {/* Share Button */}
                        <TouchableOpacity
                            style={[styles.touchWish_Share, { backgroundColor: '#eb5834' }]}
                            onPress={() => showAlertFeatureIncomplete()}>
                            <Icon
                                name="remove-circle-outline"
                                type="material-icons"
                                color="#fff"
                            />
                        </TouchableOpacity>
                    </View>

                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.viewBody}>


                        {/* Price and Type of room */}
                        <View style={styles.viewPrice_Roomtype}>
                            <Text style={styles.textPrice}>
                                VND 12/night
                            </Text>
                            <Text style={styles.textTypeRoom}>
                                phong rieng
                            </Text>
                        </View>

                        {/* Name of room */}
                        <Text style={styles.textNameRoom}>
                            Phong Tai Nha Tao
                        </Text>

                        {/* Stars and reviews*/}
                        <View style={styles.viewStar_Review}>
                            <View style={{ flexDirection: 'row' }}>
                                {this.renderStar(3)}
                            </View>

                            <TouchableOpacity onPress={() => this.gotoReview()}>
                                <View style={{ flexDirection: 'row', }}>
                                    <Text style={{ color: '#4A90E2' }}>
                                        2 reviews
                                    </Text>
                                    <Icon
                                        style={styles.iconRight}
                                        name='right'
                                        type='antdesign'
                                        color="#3389EE"
                                        size={17.5}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>

                        {/* Services */}
                        <View style={styles.viewServices}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>


                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='wifi'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Free Wifi</Text>
                                </View>

                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='free-breakfast'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Breakfast</Text>
                                </View>
                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='free-breakfast'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Breakfast</Text>
                                </View>
                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='free-breakfast'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Breakfast</Text>
                                </View>

                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='pool'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Pool</Text>
                                </View>

                                <View style={styles.viewServiceSub}>
                                    <Icon
                                        style={styles.iconRight}
                                        name='pets'
                                        type='materialIcons'
                                        color="#546375"
                                        size={17.5}
                                    />
                                    <Text>Pets</Text>
                                </View>
                            </ScrollView>

                        </View>

                        {/* Desciptions */}
                        <View style={styles.viewDescription}>
                            <Text style={styles.textDescription} numberOfLines={4}>
                                whf kjhalkdjhflwkeh klaejrhgka hriuhq4h5k1h 87 q3 84 eurtiuhertu jnncb erunsndnffgrkjge 58ugrgj3jnsdnf 35utsjf whhrugehgj  gqrj qrj qirq34t98qqr qrtjkerj34ti er qertrej
                            </Text>
                        </View>

                        {/* Location address and map*/}
                        <View style={styles.viewAddress_Map}>
                            <View style={styles.viewAddressText}>
                                <Icon
                                    style={styles.iconRight}
                                    name='location-on'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text style={styles.textAddress}>
                                    1uh4iuh jshdjfh ejhlkajsdhf
                                </Text>
                            </View>
                            {/* map view */}
                            <View style={{ height: 200, width: "100%" }}>
                                <WebView
                                    source={{ uri: "http://townhouse-cloud.dx.am/?location=" + "quang binh" }} />
                            </View>
                        </View>

                    </View>
                </ScrollView>

                {/* Book room button */}
                {/* <View style={styles.viewButtonBook}>
                    <ButtonMain nameBtn={'BOOK NOW'} />
                </View> */}
            </View>
        )
    }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },

    imageRoom: {
        width: windowWidth,
        height: 183,
        marginRight: 1.5
    },
    viewWish_Share: {
        flexDirection: 'row',
        // backgroundColor: 'grey',
        position: 'absolute',
        right: 5,
        bottom: 15
    },
    touchWish_Share: {
        marginHorizontal: 10,
        padding: 5,
        borderRadius: 15,
        // backgroundColor: '#fff'
    },

    viewPrice_Roomtype: {
        flexDirection: 'row',
        marginTop: 17,
        paddingBottom: 10,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 24,
        color: '#ff8519',
        letterSpacing: 2,

    },
    textTypeRoom: {
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 14,
        color: '#c4194a',
        letterSpacing: 2,
        textTransform: 'uppercase',
        textShadowColor: 'rgba(166, 29, 85, 0.2)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },

    textNameRoom: {
        width: 335,
        fontSize: 23,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: '#293340',

    },

    viewBody: {
        marginHorizontal: 20,
    },

    viewStar_Review: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    viewRatingStar: {
        flexDirection: 'row',
    },
    textStar: {
        fontSize: 16.5,
        color: 'orange',
    },

    viewServices: {
        flexDirection: 'row',
        paddingVertical: 11,
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    viewServiceSub: {
        marginHorizontal: 10
    },

    viewDescription: {
        paddingTop: 5.5,
    },
    textDescription: {
        fontStyle: "normal",
        fontWeight: "500",
        fontSize: 15,
        lineHeight: 22,
        color: '#39393A'
    },

    viewAddress_Map: {
        paddingTop: 5.5,
    },
    viewAddressText: {
        flexDirection: 'row',
        marginBottom: 3
    },
    textAddress: {
        fontStyle: "normal",
        fontWeight: 'bold'
    },

    addToCartTouchView: {
        height: 50,
        marginTop: 15,
        backgroundColor: '#fc9619',
    },
    touchAddToCart: {
        flex: 1,
    },
    textAddToCartTouch: {
        color: 'white',
        marginVertical: 13.5,
        textAlign: 'center',
        fontWeight: '600',
        fontSize: 16.5
    },

    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
    }
});