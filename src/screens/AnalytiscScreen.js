import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import {
    PieChart,
} from "react-native-chart-kit";
import LoaderModal from "../components/LoaderModal";
import BookingService from "../services/bookingRoom.service";

const screenWidth = Dimensions.get("window").width;

const data = [
    {
        name: "Seoul",
        population: 21500000,
        color: "rgba(131, 167, 234, 1)",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "Toronto",
        population: 1300000,
        color: "blue",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },
    {
        name: "Beijing",
        population: 527612,
        color: "red",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    },

];
export default class AnalytiscScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataStatic: [],
            totalBooking: 0,
            totalRevenue: 0
        };
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await BookingService.getBookingAnalysing().then(response => {
            this.setState({
                loading: false,
                totalBooking: response.data.data.totalBooking,
                totalRevenue: response.data.data.totalRevenue,
                dataStatic: [
                    {
                        name: "Cancelled",
                        population: response.data.data.numberOfCancel,
                        color: "red",
                        legendFontColor: "#7F7F7F",
                        legendFontSize: 15
                    },
                    {
                        name: "Completed",
                        population: response.data.data.numberOfComplete,
                        color: "green",
                        legendFontColor: "#7F7F7F",
                        legendFontSize: 15
                    },
                    {
                        name: "Staying",
                        population: response.data.data.numberOfHappening,
                        color: "yellow",
                        legendFontColor: "#7F7F7F",
                        legendFontSize: 15
                    },
                ]
            })
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error: ' + error);
        });
        this.setState({ loading: false });
    }
    render() {
        return (
            <View style={styles.mainView}>
                <LoaderModal
                    loading={this.state.loading} />
                <Text style={styles.textLogo}>
                    Statistical Booking
                </Text>

                <View style={styles.viewCheckInOutRoom}>

                    <View style={styles.viewItemInfo}>
                        <Text style={styles.textTouchLabel}>TOTAL BOOKING</Text>
                        <Text style={styles.textTouchProperty}>
                            {this.state.totalBooking}
                        </Text>
                    </View>

                    <View style={styles.viewItemInfo}>
                        <Text style={styles.textTouchLabel}>TOTAL REVENUE (VND)</Text>
                        <Text style={styles.textTouchProperty}>
                            {this.state.totalRevenue}
                        </Text>
                    </View>

                </View>

                <PieChart
                    data={this.state.dataStatic}
                    width={screenWidth}
                    height={220}
                    chartConfig={{

                        backgroundColor: '#26872a',
                        backgroundGradientFrom: '#43a047',
                        backgroundGradientTo: '#66bb6a',
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16
                        }

                    }}
                    accessor="population"
                    backgroundColor="transparent"
                    paddingLeft="15"
                    absolute
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: '#3289ee',
        // backgroundColor: '#69a6eee3',
        // backgroundColor: '#fff',
        justifyContent: 'center'
    },
    logoImg: {
        width: 170,
        height: 100,
        alignSelf: 'center',
    },
    textLogo: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 28
    },
    viewCheckInOutRoom: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 20,
        marginBottom: 15,
    },
    viewItemInfo: {
        // backgroundColor: 'red',
        flex: 1
    },
    touchProperty: {
        marginBottom: 5,
        width: 100,
        height: 60,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: 'bold',
        fontSize: 10.5,
        textAlign: "left",
        textTransform: 'uppercase',
        color: '#91929e',
        marginBottom: 2,
        textAlign: 'center'
    },
    textTouchProperty:{
        textAlign: 'center'
    }
});