import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList, ScrollView, SafeAreaView } from "react-native";
import { Icon } from "react-native-elements";

import { mStyles } from "../styles/styles";
import CardReviews from "../components/CardReviews";
import { showAlertFeatureIncomplete } from "../utils/alert.utils";


const reviewsData = [
    {
        id: 1,
        title: 'Nice View',
        stars: 4,
        name: 'Bé Bé',
        date: 'Jan 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 2,
        title: 'Nice View',
        stars: 4,
        name: 'Bé Bé',
        date: 'Jan 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 3,
        title: 'Nice View',
        stars: 4,
        name: 'Bé Bé',
        date: 'Jan 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 4,
        title: 'Great View',
        stars: 5,
        name: 'Bé Bé2',
        date: 'Feb 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 5,
        title: 'Tàm Tạm View',
        stars: 3,
        name: 'Bé Bé3',
        date: 'Mar 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 6,
        title: 'Ok View',
        stars: 3,
        name: 'Bé Bé3',
        date: 'Mar 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
    {
        id: 7,
        title: 'Dở Ẹt View',
        stars: 1,
        name: 'Bé Bé3',
        date: 'Mar 2020',
        content: 'Chỗ này view khá đẹp, nói chung là không biết viết sao cho thật nhiều chữ để test tính năng này, vậy nên đang ráng viết cho dài dòng'
    },
]

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderRowStars = () => {

        var starArr = [];
        var starElements = [];

        var votingNumStar = [2, 1, 2, 5, 3];
        var sumVoting = votingNumStar.reduce((a, b) => a + b, 0);

        console.log('sum voting: ', sumVoting);
        for (let i = 5; i > 0; i--) {

            for (let j = i; j > 0; j--) {
                starElements.push('✩');
            }

            starArr.push(
                <View style={{ flexDirection: 'row', width: '100%' }} key={i}>
                    {/* stars */}
                    <Text style={{ fontSize: 10, width: '20%', textAlign: 'right', marginRight: 2.5 }}>
                        {starElements}
                    </Text>
                    {/* progress bar */}
                    <View style={{ justifyContent: 'center', width: '80%', marginLeft: 2.5 }}>
                        <View style={{ height: 3.5, width: '100%', backgroundColor: '#fff' }}>
                            <View style={{ height: 3.5, width: (votingNumStar[i - 1] / (sumVoting) * 100) + '%', backgroundColor: '#3389EE' }} />
                        </View>
                    </View>
                </View>
            );
            starElements = [];
        }

        return starArr;
    }

    renderReviews = (reviews) => {
        return (
            <FlatList
                // contentContainerStyle={{paddingBottom: 90}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={reviews}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardReviews
                            title={item.title}
                            stars={item.stars}
                            name={item.name}
                            dateReview={item.dateReview}
                            content={item.content} />
                }
            />
        )
    }
    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                {/* top view */}
                <View style={styles.viewTop}>

                    {/* Reviews, write a review button */}
                    <View style={styles.viewRowTop}>

                        <Text style={styles.textReviewTitle}>
                            Reviews
                        </Text>

                        <TouchableOpacity
                            style={styles.touchWriteReview}
                            onPress={() => showAlertFeatureIncomplete()}>
                            <View style={{ flexDirection: 'row', }}>
                                <Text style={{ color: '#4A90E2' }}>
                                    Write a review
                                    </Text>
                                <View style={{ justifyContent: 'center' }}>

                                    <Icon
                                        name='right'
                                        type='antdesign'
                                        color="#3389EE"
                                        size={16.5}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {/* total stars,  */}
                    <View style={styles.viewRowTop}>

                        {/* left */}
                        <View style={{ flex: 1 }}>

                            <Text style={styles.textAvgStar}>
                                4.5
                            </Text>
                            <Text style={styles.textOutOf5}>
                                (OUT OF 5)
                            </Text>
                        </View>

                        {/* right */}
                        <View style={{ flex: 2, }}>

                            {/* stars matrix */}
                            <View style={{ width: '100%' }}>

                                {/* row star */}
                                {this.renderRowStars()}

                            </View>

                            {/* Toto rating(reviews) */}
                            <Text style={styles.textTotalReview}>
                                25 Ratings
                            </Text>
                        </View>
                    </View>
                </View>

                {/* body: card reviews */}

                <View style={styles.viewListReview}>

                        {this.renderReviews(reviewsData)}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        paddingHorizontal: 20
    },
    viewRowTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'blue'
    },
    textReviewTitle: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 26,
        lineHeight: 32,
        color: '#24253D'
    },
    touchWriteReview: {
        justifyContent: 'center'
    },

    textAvgStar: {
        textAlign: 'center',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 70,
        lineHeight: 75,
        color: '#F79F27'
    },
    textOutOf5: {
        textAlign: 'center',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        color: '#91929e'
    },

    textTotalReview: {
        textAlign: 'right',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 13,
        color: '#24253D'
    },

    viewListReview: {
        marginTop: 10,
        flex: 1
    },

});