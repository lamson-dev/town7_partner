import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Calendar } from 'react-native-calendars';
import { Icon } from "react-native-elements";
import BookingRoomService from "../services/bookingRoom.service";
import LoaderModal from "../components/LoaderModal";

const nowDate = new Date();
export default class BookingCalendarScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      bookingData: props.bookingDataProps,
      currentDate: nowDate.getFullYear() + '-' + ((nowDate.getMonth() + 1) < 10 ? '0' + (nowDate.getMonth() + 1) : (nowDate.getMonth() + 1)) + '-' + nowDate.getDate(),

      daySeleted: null,

      listDateCheckDot: {},

      listDate: {},

      listCheckItemCalendar: [],

      checkinDot: {
        key: 'checkinDot',
        color: 'green',
        selectedDotColor: 'blue'
      },

      checkoutDot: {
        key: 'checkoutDot',
        color: '#c80b0b'
      },
      enableScrollViewScroll: false,
      loading: false
    }
  }

  setDateSeleted = (day) => {

    console.log('select date: ', day);
    if (this.state.listDate.hasOwnProperty(day)) {

      console.log(this.state.listDate[day]);

      let tempListCheckDate = [];
      if (this.state.listDate[day].hasOwnProperty('checkin') && this.state.listDate[day].hasOwnProperty('checkout')) {

        // this.state.listDate[day].checkin.forEach((element, index) => {
        //     console.log('el in: ', element)
        //     tempListCheckDate.push({
        //         id: index,
        //         guestName: element.user.name,
        //         status: 'checkin',
        //         nights: element.nights,
        //         guests: element.guests
        //     });
        // });

        this.state.listDate[day].checkout.forEach((element, index) => {
          console.log('el out: ', element)
          tempListCheckDate.push({
            id: index,
            guestName: element.user.name,
            status: 'checkout',
            nights: element.nights,
            guests: element.guests
          });
        });

        this.setState({ listCheckItemCalendar: tempListCheckDate });
      }
    }
    // this.setState({
    //     daySeleted: day
    // });
  }

  renderBookingCalendar = (bookings) => {

    return (
      <FlatList
        // horizontal={true}
        // nestedScrollEnabled={true}
        scrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        // showsVerticalScrollIndicator={false}
        data={bookings}
        keyExtractor={(item, index) => item.id.toString()}
        renderItem={
          ({ item }) =>
            this.renderBookingCalendarElement({
              name: "balbal",
              guests: item.guests
            })
        }
      />
    )
  }

  async componentDidMount() {
    this.setState({ loading: true });
    await BookingRoomService.getBookingCalendar()
      .then(response => {


        this.setState({ listDate: response.data });

        var dataArr = {
          // '2020-08-16': {
          //     selected: true,
          //     dots: [this.state.checkinDot, this.state.checkoutDot],
          // },
          // '2020-08-17': {
          //     selected: true,
          //     dots: [this.state.checkinDot, this.state.checkoutDot],
          // },
        };

        for (const key in this.state.listDate) {

          if (this.state.listDate[key].hasOwnProperty('checkin') && !this.state.listDate[key].hasOwnProperty('checkout')) {
            console.log('co in');
            console.log(key);
            console.log(this.state.listDate[key]);
            dataArr[key] = {
              dots: [this.state.checkinDot],
            };

          } else if (this.state.listDate[key].hasOwnProperty('checkout') && !this.state.listDate[key].hasOwnProperty('checkin')) {
            console.log('co out');
            console.log(key);
            console.log(this.state.listDate[key]);
            dataArr[key] = {
              dots: [this.state.checkoutDot],
            };

          } else {
            console.log('co in va out');
            console.log(key);
            console.log(this.state.listDate[key]);
            dataArr[key] = {
              dots: [this.state.checkinDot, this.state.checkoutDot],
            };
          }
        }

        this.setState({
          listDateCheckDot: dataArr,
          loading: false
        });

      }).catch(error => {
        this.setState({ loading: false });
        alert('Error: ', error);
      });

    this.setState({ loading: false });



  }

  renderBookingCalendarElement = ({ guestName, status, nights, guests }) => {
    return (

      <View style={styles.viewBookingElement}>
        {status == 'checkin' ?
          <Icon
            name="login"
            type="entypo"
            color="#24c3f0"
          /> : <Icon
            name="log-out"
            type="entypo"
            color="#6e6e6e"
          />}

        <TouchableOpacity>
          <View style={styles.viewName_Night}>
            {/* Guest name */}
            <Text style={styles.textGuestName}>{guestName}</Text>
            <Text>{nights} nights - {guests} guest</Text>
          </View>

        </TouchableOpacity>
      </View>
    )
  }

  renderItem = (item, index) => {
    return (
      <Text key={index}>
        {item}
      </Text>
    )
  }

  renderCalendar = () => {
    return (
      <Calendar
        // Initially visible month. Default = Date()
        // current={this.state.currentDate}
        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
        // minDate={this.state.currentDate}
        // minDate={'2020-04-12'}
        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
        // maxDate={'2012-05-30'}
        // Handler which gets executed on day press. Default = undefined
        // onDayPress={(day) => { this.setDateSeleted(this.state.dateStartSeleted == null ? 'start' : 'end', day.dateString) }}
        onDayPress={(day) => { this.setDateSeleted(day.dateString) }}
        // Handler which gets executed on day long press. Default = undefinedzbu
        // onDayLongPress={(day) => { console.log('selected day-long', day) }}
        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
        // monthFormat={'yyyy MM dd'}
        // Handler which gets executed when visible month changes in calendar. Default = undefined
        onMonthChange={(month) => { console.log('month changed', month) }}
        // Hide month navigation arrows. Default = false
        hideArrows={false}
        // Replace default arrows with custom ones (direction can be 'left' or 'right')
        // renderArrow={(direction) => (<Arrow />)}
        // Do not show days of other months in month page. Default = false
        hideExtraDays={true}
        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
        // day from another month that is visible in calendar page. Default = false
        disableMonthChange={true}
        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
        firstDay={1}
        // Hide day names. Default = false
        // hideDayNames={true}
        // Show week numbers to the left. Default = false
        showWeekNumbers={false}
        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
        onPressArrowLeft={substractMonth => substractMonth()}
        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
        onPressArrowRight={addMonth => addMonth()}
        // Disable left arrow. Default = false
        disableArrowLeft={false}
        // Disable right arrow. Default = false
        disableArrowRight={false}
        markedDates={
          this.state.listDateCheckDot
        }
        // markedDates={{
        //     // [this.state.startDate]: {
        //     //     startingDay: true,
        //     //     selected: true,
        //     //     disableTouchEvent: true,
        //     //     color: '#24c3f0'
        //     // },
        //     // '2020-08-26': {
        //     //     selected: true,
        //     //     disableTouchEvent: true,
        //     //     color: '#24c3f0'
        //     // },
        //     // '2020-08-27': {
        //     //     selected: true,
        //     //     disableTouchEvent: true,
        //     //     color: '#24c3f0'
        //     // },
        //     // [this.state.endDate]: {
        //     //     endingDay: true,
        //     //     selected: true,
        //     //     disableTouchEvent: true,
        //     //     color: '#24c3f0'
        //     // },

        //     // '2020-08-16': {
        //     //     selected: true,
        //     //     selectedColor: '#f5f5f5',
        //     //     dots: [this.state.checkinDot, this.state.checkoutDot],
        //     // },
        //     // '2020-08-25': {
        //     //     dots: [this.state.checkinDot, this.state.checkoutDot],
        //     // },
        // }}
        markingType={'multi-dot'}
        theme={{
          monthTextColor: '#24c3f0',
        }}
      />
    );
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <LoaderModal
          loading={this.state.loading} />

        <FlatList
          ListHeaderComponent={this.renderCalendar}
          data={() => this.state.listCheckItemCalendar}
          keyExtractor={(item, index) => item.id.toString()}
          renderItem={
            ({ item }) => {
              <View style={styles.viewBodyInfo}>
                {/* heading body */}

                <View style={styles.viewHeadingInfo}>

                  <View style={styles.viewLabelHeading}>
                    <Text style={styles.textLabeHeading}>Arrivals</Text>
                    <Text>2</Text>
                  </View>

                  <View style={styles.viewLabelHeading}>
                    <Text style={styles.textLabeHeading}>Departures</Text>
                    <Text>2</Text>
                  </View>

                </View>

                {/* Main body */}
                <View style={styles.viewMainBodyInfo}>
                  {/* booking calendar elements */}
                </View>

              </View>
            }
          }
        />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  viewBodyInfo: {
    marginHorizontal: 5,
    // borderColor: 'black',

  },
  viewHeadingInfo: {
    backgroundColor: '#fff',
    marginTop: 5,
    marginBottom: 2.5,
    // backgroundColor: 'red',
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewLabelHeading: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLabeHeading: {
    color: 'grey',
    fontWeight: '700'
  },

  viewMainBodyInfo: {

  },
  viewBookingElement: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    marginBottom: 2
  },
  viewName_Night: {
    paddingLeft: 20
  },
  textGuestName: {
    fontWeight: '700'
  }

});