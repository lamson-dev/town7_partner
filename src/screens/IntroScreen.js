import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";

export default class IntroScreen extends Component {
    render() {
        return (
            <View style={styles.mainView}>
                <View>
                    <Image style={styles.logoImg} source={require('../assets/images/houses-for-sale-in-png-5.png')} ></Image>
                </View>
                <Text style={styles.textLogo}>
                    Town House
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: '#3289ee',
        backgroundColor: '#69a6eee3',
        // backgroundColor: '#fff',
        justifyContent: 'center'
    },
    logoImg: {
        width: 170,
        height: 100,
        alignSelf: 'center',
    },
    textLogo:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 28
    }
});