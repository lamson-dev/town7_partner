import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, Picker } from "react-native";
import { Navigation } from "react-native-navigation";

import FabButton from "../../components/FabButton";
import TextInputComp from "../../components/TextInputComp";
import LoaderModal from "../../components/LoaderModal";
import RoomGuestScreen from "./RoomGuestScreen";
import HostCityService from "../../services/hostCity.service";

export default class RoomAddressScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            roomData: props.roomData,

            address: null,
            hostHostCityId: null,

            hostHostCityItems: null,
            loading: false
        }
    }

    submit = () => {
        var roomDataTemp = this.state.roomData;
        roomDataTemp['hostCityId'] = this.state.hostHostCityId;
        roomDataTemp['address'] = this.state.address;

        this.setState({ roomData: roomDataTemp });

        this.goToNext();
    }

    goToNext = () => {

        Navigation.registerComponent('RoomGuestScreen', () => RoomGuestScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'RoomGuestScreen',
                            passProps: {
                                roomData: this.state.roomData
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'Guests',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }

                    }
                ]
            }
        })
    }

    setHostCityId = (value) => {
        this.setState({ hostHostCityId: value });
    }

    renderPickerItem = (data) => {
        let pickerItems = [];
        data.forEach(element => {
            pickerItems.push(<Picker.Item
                key={element.id}
                label={element.name}
                value={element.id}
            />);
        });
        this.setState({ hostHostCityItems: pickerItems });
    }

    async componentDidMount() {
        console.log('rom data: ', this.state.roomData);
        this.setState({ loading: true });

        var response = await HostCityService.getHostCities();
        this.renderPickerItem(response.data.data);
        this.setState({
            loading: false
        });

    }

    render() {
        return (
            <View style={styles.viewMain}>
                <LoaderModal
                    loading={this.state.loading} />
                <Text style={styles.textHeading}>Where is your room?</Text>

                <View style={styles.viewBody}>
                    <ScrollView showsVerticalScrollIndicator={false}>


                        <View style={styles.viewField}>

                            <Text style={{ color: '#9ea0a4', marginVertical: 5 }}>
                                City/Pronvice
                            </Text>
                            <Picker selectedValue={this.state.hostHostCityId} onValueChange={this.setHostCityId}>
                                {this.state.hostHostCityItems}
                            </Picker>
                        </View>

                        <View style={styles.viewField}>

                            <Text style={{ color: '#9ea0a4', marginVertical: 5 }}>
                                Address
                            </Text>
                            <TextInputComp
                                onChangeText={(value) => this.setState({ address: value })} />
                        </View>

                    </ScrollView>

                    <FabButton title="NEXT" onPress={this.submit} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },
    textHeading: {
        color: '#474747',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    viewBody: {
        marginHorizontal: 5,
        paddingTop: 15,
        flex: 1,
    },
    viewField: {
        paddingTop: 5
    }

});