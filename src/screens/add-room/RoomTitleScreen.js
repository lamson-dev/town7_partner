import React, { Component } from "react";
import { View, StyleSheet, TextInput, ScrollView, Picker, Text } from "react-native";
import { Navigation } from "react-native-navigation";
import CheckBox from "@react-native-community/checkbox";

import FabButton from "../../components/FabButton";
import TextInputComp from "../../components/TextInputComp";
import RoomAddressScreen from "./RoomAddressScreen";
import LoaderModal from "../../components/LoaderModal";
import HostCategoryService from "../../services/hostCategory.service";

export default class RoomTitleScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            languageItems: [
                {
                    id: 1,
                    name: "Tiếng Việt",
                    checked: false
                },
            ],
            hostCategoryId: null,

            hostCategoryItems: null,
            loading: false,
        }
    }

    submit = () => {
        this.goToNext();
    }

    goToNext = () => {

        Navigation.registerComponent('RoomAddressScreen', () => RoomAddressScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'RoomAddressScreen',
                            passProps: {
                                roomData: {
                                    name: this.state.name,
                                    description: this.state.description,
                                    hostCategoryId: this.state.hostCategoryId,
                                    languageIds: this.state.languageIds
                                }
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'Room Address',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        })

    }

    setHostCategoryId = (value) => {
        this.setState({ hostCategoryId: value });
    }

    renderPickerItem = (data) => {
        let pickerItem = [];
        data.forEach((element, index) => {
            pickerItem.push(<Picker.Item
                key={index}
                label={element.name}
                value={element.id}
            />);
        });
        this.setState({ hostCategoryItems: pickerItem });
    }

    onChangeCheckbox = (index, value) => {

        var tempCheckboxes = this.state.languageItems;
        var tempLanguageIds = [];

        tempCheckboxes[index].checked = value;

        this.setState({ languageItems: tempCheckboxes });

        this.state.languageItems.forEach(element => {
            if (element.checked) {
                tempLanguageIds.push(element.id);
            }
        });

        this.setState({ languageIds: tempLanguageIds });
    }

    async componentDidMount() {

        this.setState({ loading: true });

        var response = await HostCategoryService.getHostCategories();
        this.renderPickerItem(response.data.data);

        this.setState({
            loading: false
        });

    }

    render() {
        const { languageItems } = this.state;
        return (
            <View style={styles.viewMain}>
                <LoaderModal
                    loading={this.state.loading} />
                <View style={styles.viewBody}>
                    <ScrollView>
                        <TextInputComp
                            placeholder={'Room Title'}
                            onChangeText={(value) => this.setState({ name: value })} />

                        <View style={styles.viewTextAreaContainer} >
                            <TextInput
                                style={styles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder="Type something"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                                onChangeText={(value) => this.setState({ description: value })}
                            />
                        </View>

                        <View style={styles.viewField}>

                            <Text style={styles.textHeadingField}>What category type of your host?</Text>
                            <Picker selectedValue={this.state.hostCategoryId} onValueChange={this.setHostCategoryId}>
                                {this.state.hostCategoryItems}
                            </Picker>
                        </View>

                        <View style={styles.viewField}>

                            <Text style={styles.textHeadingField}>Languages?</Text>
                            {languageItems.map((checkbox, index) => (

                                <View style={styles.viewCheck} key={index}>
                                    <CheckBox
                                        key={index}
                                        value={checkbox.checked}
                                        onValueChange={(value) => this.onChangeCheckbox(index, value)} />
                                    <Text>{checkbox.name}</Text>
                                </View>
                            ))}
                        </View>

                    </ScrollView>
                    <FabButton title="NEXT" onPress={this.submit} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },
    viewBody: {
        marginHorizontal: 5,
        paddingTop: 15,
        flex: 1,
    },
    viewTextAreaContainer: {
        marginTop: 10,
        borderColor: '#E0E0E1',
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
    },
    textArea: {
        height: 150,
        textAlignVertical: "top"
    },
    viewCheck: {
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textHeading: {
        color: '#474747',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    viewField: {
        paddingTop: 5
    },
    textHeadingField: {
        color: '#9ea0a4',
        marginVertical: 5
    }
});