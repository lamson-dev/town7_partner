import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, } from "react-native";
import { Navigation } from "react-native-navigation";
import CheckBox from "@react-native-community/checkbox";

import FabButton from "../../components/FabButton";
import { mStyles } from "../../styles/styles";
import RoomRulesScreen from "./RoomRulesScreen";

export default class FacilitiesScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            roomData: props.roomData,

            facilities: [
                {
                    title: 'isHasElevator',
                    checked: false
                },
                {
                    title: 'isHasGym',
                    checked: false
                },
                {
                    title: 'isHasBar',
                    checked: false
                },
                {
                    title: 'isHasCarPark',
                    checked: false
                },
                {
                    title: 'isHasLaundry',
                    checked: false
                }
            ],
        }
    }

    submit = () => {
        var roomDataTemp = this.state.roomData;

        this.state.facilities.forEach(element => {
            if (element.checked == true) {
                roomDataTemp[element.title] = element.checked;
            }
        });


        this.setState({ roomData: roomDataTemp });

        this.goToNext();
    }

    goToNext = () => {

        Navigation.registerComponent('RoomRulesScreen', () => RoomRulesScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'RoomRulesScreen',
                            passProps: {
                                roomData: this.state.roomData
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'House Rules',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        })
    }

    onChange = (index, value) => {

        var tempCheckboxes = this.state.facilities;

        tempCheckboxes[index].checked = value;

        this.setState({ checkboxes: tempCheckboxes });

    }

    render() {
        const { facilities } = this.state;
        return (
            <View style={styles.viewMain}>
                <View style={mStyles.viewBody}>
                    <ScrollView>
                        <Text style={mStyles.textHeading}>What can guests use at your place ?</Text>

                        {facilities.map((checkbox, index) => (

                            <View style={styles.viewCheck} key={index}>
                                <CheckBox
                                    key={index}
                                    value={checkbox.checked}
                                    onValueChange={(value) => this.onChange(index, value)} />
                                <Text>{checkbox.title}</Text>
                            </View>
                        ))}
                    </ScrollView>
                </View>
                <FabButton title="NEXT" onPress={this.submit} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },

    viewCheck: {
        // backgroundColor: 'blue',
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: '#fff'
    }

});