import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, } from "react-native";
import { Navigation } from "react-native-navigation";

import TextInputComp from "../../components/TextInputComp";
import FabButton from "../../components/FabButton";
import HostService from "../../services/host.service";
import LoaderModal from "../../components/LoaderModal";

export default class RoomPriceScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            roomData: props.roomData,
            standardPriceMondayToThursday: 0,

            loading: false
        }
    }

    submit = async () => {
        var roomDataTemp = this.state.roomData;
        roomDataTemp['standardPriceMondayToThursday'] = this.state.standardPriceMondayToThursday;
        this.setState({ roomData: roomDataTemp });
        console.log('data now: ', this.state.roomData);

        this.setState({ loading: true });
        await HostService.createHost(this.state.roomData).then(() => {
            this.setState({ loading: false });
            Navigation.popToRoot(this.props.componentId);
            Navigation.showModal({
                stack: {
                    children: [
                        {
                            component: {
                                name: "SuccessfulRoomScreen"
                            }
                        }
                    ]
                }
            })
        }).catch(error => {
            this.setState({ loading: false });
            alert('Can not create your room.\n Please check you information again')
        });

        // this.goToNext();
    }

    render() {
        return (
            <View style={styles.viewMain}>
                <LoaderModal
                    loading={this.state.loading} />
                <View style={styles.viewBody}>
                    <ScrollView>
                        <View style={styles.viewField}>
                            <Text style={styles.textHeading}>
                                How much do you want to charge per night?
                            </Text>
                            <TextInputComp placeholder="(VND) Price guests pay" keyboardType="numeric"
                                onChangeText={(value) => this.setState({ standardPriceMondayToThursday: value })} />
                        </View>

                    </ScrollView>
                    <FabButton title="PUBLISH NOW" onPress={this.submit} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },
    viewBody: {
        marginHorizontal: 5,
        paddingTop: 15,
        flex: 1,
    },
    textHeading: {
        color: '#474747',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    viewField: {
        paddingTop: 5
    },
    textHeadingField: {
        color: '#9ea0a4',
        marginVertical: 5
    }
});