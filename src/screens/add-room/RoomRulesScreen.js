import React, { Component } from "react";
import { View, Text, StyleSheet, Switch, ScrollView, } from "react-native";
import { Navigation } from "react-native-navigation";
import CheckBox from "@react-native-community/checkbox";

import FabButton from "../../components/FabButton";
import { mStyles } from "../../styles/styles";
import LoaderModal from "../../components/LoaderModal";
import RuleService from "../../services/rule.service";
import RoomPriceScreen from "../../screens/add-room/RoomPriceScreen";

export default class RoomRulesScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            roomData: props.roomData,

            ruleItems: [],

            loading: false,
        }
    }

    submit = () => {
        var roomDataTemp = this.state.roomData;
        roomDataTemp['ruleIds'] = this.state.ruleIds
        this.setState({ roomData: roomDataTemp });

        this.goToNext();
    }

    goToNext = () => {

        Navigation.registerComponent('RoomPriceScreen', () => RoomPriceScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'RoomPriceScreen',
                            passProps: {
                                roomData: this.state.roomData
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'Price',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        })
    }

    onChangeCheckbox = (index, value) => {

        var tempRuleItems = this.state.ruleItems;
        var tempRuleIds = [];

        tempRuleItems[index].checked = value;

        this.setState({ ruleItems: tempRuleItems });

        this.state.ruleItems.forEach(element => {
            if (element.checked) {
                tempRuleIds.push(element.id);
            }
        });

        this.setState({ ruleIds: tempRuleIds });
    }

    async componentDidMount() {

        this.setState({ loading: true });

        var response = await RuleService.getRules();

        var tempRules = [];

        response.data.data.forEach(element => {
            tempRules.push({
                id: element.id,
                name: element.name,
                checked: false
            })
        });

        this.setState({
            ruleItems: tempRules,
            loading: false
        });

    }

    render() {
        const { ruleItems } = this.state;
        return (
            <View style={styles.viewMain}>
                <LoaderModal
                    loading={this.state.loading} />
                <View style={mStyles.viewBody}>
                    <ScrollView>
                        <Text style={mStyles.textHeading}>
                            House Rules
                            </Text>

                        {ruleItems.map((checkbox, index) => (

                            <View style={styles.viewCheck} key={index}>
                                <CheckBox
                                    key={index}
                                    value={checkbox.checked}
                                    onValueChange={(value) => this.onChangeCheckbox(index, value)} />
                                <Text>{checkbox.name}</Text>
                            </View>
                        ))}
                    </ScrollView>
                </View>
                <FabButton title="NEXT" onPress={this.submit} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },
    viewCheck: {
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textHeading: {
        color: '#474747',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});