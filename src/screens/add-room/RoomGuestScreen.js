import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, } from "react-native";
import { Navigation } from "react-native-navigation";

import FabButton from "../../components/FabButton";
import { mStyles } from "../../styles/styles";
import FacilitiesScreen from "./FacilitiesScreen";

export default class RoomGuestScreenScreen extends Component {


    constructor(props) {
        super(props);
        this.state = {
            roomData: props.roomData,
            adults: 1,
            children: 0,
            infants: 0,
            bedroom: 0,
            bathroom: 0
        }
    }

    submit = () => {
        var roomDataTemp = this.state.roomData;
        roomDataTemp['numberOfAdultGuest'] = this.state.adults;
        roomDataTemp['numberOfChildrenGuest'] = this.state.children;
        roomDataTemp['numberOfInfantGuest'] = this.state.infants;
        roomDataTemp['numberOfMaximumGuest'] = this.state.infants + this.state.children + this.state.adults;
        roomDataTemp['bedroomCount'] = this.state.bedroom;
        roomDataTemp['bathroomCount'] = this.state.bedroom;

        this.setState({ roomData: roomDataTemp });
        this.goToNext();
    }

    goToNext = () => {

        Navigation.registerComponent('FacilitiesScreen', () => FacilitiesScreen);
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: 'FacilitiesScreen',
                            passProps: {
                                roomData: this.state.roomData
                            },
                            options: {
                                topBar: {
                                    visible: true,
                                    background: {
                                        color: '#1ab3c7'
                                    },
                                    title: {
                                        text: 'Facilities',
                                        color: '#fff'
                                    },
                                    backButton: {
                                        color: '#fff'
                                    }
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        })
    }

    increaseValue(type) {
        if (type == 'adults') {
            this.setState({ adults: this.state.adults + 1 });

        } else if (type == 'children') {
            this.setState({ children: (this.state.children + 1) });

        } else if (type == 'infants') {
            this.state.infants < 5 ? this.setState({ infants: (this.state.infants + 1) }) : null;
        } else if (type == 'bed') {
            this.setState({ bedroom: this.state.bedroom + 1 })
        } else {
            this.setState({ bathroom: this.state.bathroom + 1 })
        }
    }

    reduceValue(type) {
        if (type == 'adults') {
            this.state.adults > 1 ? this.setState({ adults: this.state.adults - 1 }) : null;
        } else if (type == 'children') {
            this.state.children > 0 ? this.setState({ children: (this.state.children - 1) }) : null;
        } else if (type == 'infants') {
            this.state.infants > 0 ? this.setState({ infants: (this.state.infants - 1) }) : null;
        } else if (type == 'bed') {
            this.state.bedroom > 0 ? this.setState({ bedroom: (this.state.bedroom - 1) }) : null;
        } else {
            this.state.bathroom > 0 ? this.setState({ bathroom: (this.state.bathroom - 1) }) : null;
        }
    }

    render() {
        return (
            <View style={styles.viewMain}>
                <View style={mStyles.viewBody}>
                    <ScrollView>
                        <Text style={mStyles.textHeading}>How many guests can stay?</Text>

                        <View style={styles.viewChildModal}>
                            {/* view body select number of people */}
                            <View style={styles.viewBodySelect}>

                                {/* Row: Adults */}
                                <View style={styles.viewRowSelect}>

                                    {/* Title, age */}
                                    <View style={styles.viewTitleSelect}>
                                        <Text>Adults</Text>
                                        <Text>13+ years</Text>
                                    </View>

                                    {/* Button increase or reduce */}
                                    <View style={styles.viewButtonsIncrRedu}>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('adults')}>
                                            <Text style={styles.markIncrRedu}>-</Text>
                                        </TouchableOpacity>

                                        <Text style={styles.textNumberIncrRedu}>
                                            {this.state.adults}
                                        </Text>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('adults')}>
                                            <Text style={styles.markIncrRedu}>+</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                {/* Row: Children */}
                                <View style={styles.viewRowSelect}>

                                    {/* Title, age */}
                                    <View style={styles.viewTitleSelect}>
                                        <Text>Children</Text>
                                        <Text>2 - 12 years</Text>
                                    </View>

                                    {/* Button increase or reduce */}
                                    <View style={styles.viewButtonsIncrRedu}>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('children')}>
                                            <Text style={styles.markIncrRedu}>-</Text>
                                        </TouchableOpacity>

                                        <Text style={styles.textNumberIncrRedu}>
                                            {this.state.children}
                                        </Text>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('children')}>
                                            <Text style={styles.markIncrRedu}>+</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                {/* Row: Infants */}
                                <View style={styles.viewRowSelect}>

                                    {/* Title, age */}
                                    <View style={styles.viewTitleSelect}>
                                        <Text>Infants</Text>
                                        <Text>under 2 years</Text>
                                    </View>

                                    {/* Button increase or reduce */}
                                    <View style={styles.viewButtonsIncrRedu}>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('infants')}>
                                            <Text style={styles.markIncrRedu}>-</Text>
                                        </TouchableOpacity>

                                        <Text style={styles.textNumberIncrRedu}>
                                            {this.state.infants}
                                        </Text>

                                        <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('infants')}>
                                            <Text style={styles.markIncrRedu}>+</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>

                            <View style={{ marginTop: 25 }}>
                                <Text>
                                    8 guests maximum, infants don't count toward the number of guests.
                            </Text>
                            </View>
                        </View>

                        <View>
                            <Text style={mStyles.textHeading}>How many bedroom and bathroom are there?</Text>
                            <View style={styles.viewChildModal}>

                                {/* view body select number of people */}
                                <View style={styles.viewBodySelect}>

                                    {/* Row: Bedroom */}
                                    <View style={styles.viewRowSelect}>

                                        {/* Title, age */}
                                        <View style={styles.viewTitleSelect}>
                                            <Text>Bedroom</Text>
                                        </View>

                                        {/* Button increase or reduce */}
                                        <View style={styles.viewButtonsIncrRedu}>

                                            <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('bed')}>
                                                <Text style={styles.markIncrRedu}>-</Text>
                                            </TouchableOpacity>

                                            <Text style={styles.textNumberIncrRedu}>
                                                {this.state.bedroom}
                                            </Text>

                                            <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('bed')}>
                                                <Text style={styles.markIncrRedu}>+</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </View>

                                    {/* Row: Bathroom */}
                                    <View style={styles.viewRowSelect}>

                                        {/* Title, age */}
                                        <View style={styles.viewTitleSelect}>
                                            <Text>Bathroom</Text>
                                        </View>

                                        {/* Button increase or reduce */}
                                        <View style={styles.viewButtonsIncrRedu}>

                                            <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('bath')}>
                                                <Text style={styles.markIncrRedu}>-</Text>
                                            </TouchableOpacity>

                                            <Text style={styles.textNumberIncrRedu}>
                                                {this.state.bathroom}
                                            </Text>

                                            <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('bath')}>
                                                <Text style={styles.markIncrRedu}>+</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </View>



                                </View>

                            </View>
                        </View>
                    </ScrollView>
                </View>
                <FabButton title="NEXT" onPress={this.submit} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },

    viewChildModal: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    viewActionTouch: {
        // backgroundColor: '#383bff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#E0E0E1",
    },
    toucClose: {
        flex: 1,
        justifyContent: 'center',
    },

    viewBodySelect: {
        paddingHorizontal: 15,
    },
    viewRowSelect: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#E0E0E1",
    },
    viewTitleSelect: {
        // backgroundColor: 'orange'
    },
    viewButtonsIncrRedu: {
        width: 95,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    touchIncrRedu: {
        backgroundColor: '#3389EE',
        width: 25,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25
    },
    markIncrRedu: {
        fontSize: 25,
        color: 'white',
    },
    textNumberIncrRedu: {
        fontSize: 25,
        marginHorizontal: 10,
    }
});