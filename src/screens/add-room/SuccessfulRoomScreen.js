import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../../components/ButtonMain";
import { mStyles } from "../../styles/styles";

export default class SuccessfulRoomScreen extends Component {

    onContinuePress = () => {
        // Navigation.dismissModal(this.props.componentId);
        Navigation.dismissAllModals();
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                <View>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        You're now live on Town 7
                    </Text>

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontSize: 19.5, textAlign: 'center' }}>
                            Waiting for our admin confirm your information as soon.
                        </Text>
                    </View>
                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'MANAGE MY ROOMS'} onPress={this.onContinuePress} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

});