import React from 'react';
import { View, StyleSheet, ActivityIndicator, Image, } from 'react-native';
import { connect } from "react-redux";

import { goToLogin, showSideTabsMenu } from '../navigator/navigation';
import deviceStorage from "../services/deviceStorage.service";
import { fetchUserDataService } from "../services/fetchUserData.service";

class InitialisingScreen extends React.Component {

    constructor(props) {
        super(props);
    }
    static options() {
        return {
            statusBar: {
                backgroundColor: '#3389EE',
                style: "light"
            }
        };

    }

    async componentDidMount() {
        await fetchUserDataService().then((response) => {
            showSideTabsMenu(response.data.data)
        }).catch(error => {
            console.log(error);
            goToLogin()
        });
    }

    render() {
        const { isLoggedIn } = this.props;
        console.log('-init login status: ', isLoggedIn);
        return (
            <View style={styles.container}>
                <Image style={styles.imgLogo} source={require('../assets/images/Logo-app.png')}></Image>
                <ActivityIndicator size="large" color="#fff" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textWelcome: {
        fontSize: 28,
        color: '#fff'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3389EE'
    },
    viewLogo: {
        marginVertical: 5
    },
    imgLogo: {
        width: 285,
        height: 285,
        alignSelf: 'center',
    },
})

mapStateToProps = (state) => ({
    // isLoggedIn: state.authUser.isLoggedIn
})

export default connect(mapStateToProps, null)(InitialisingScreen)