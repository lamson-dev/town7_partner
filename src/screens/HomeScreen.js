import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, } from "react-native";

import { Navigation } from "react-native-navigation";
import { Icon } from "react-native-elements";

import NotificationService from "../services/notification.service";
import UserService from "../services/user.service";

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: {
                email: '',
                fullName: '',
                phone: ''
            },
            totalNotificationUnRead: 0
        };
    }

    goToListingProperties = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ListPropertiesScreen',
                options: {
                    statusBar: {
                        backgroundColor: '#1ab3c7',
                        style: 'light'
                    },
                    topBar:{
                        title:{
                            text: 'Listting Properties'
                        }
                    }
                }
            }
        });
    }

    goToBookingManagement = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'BookingManagementScreen',
                options: {
                    topBar: {
                        title: {
                            text: 'Booking Management',

                        }
                    }
                }
            }
        });
    }

    goToBookingCalendar = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'BookingCalendarScreen',
                options: {
                    topBar: {
                        title: {
                            text: 'Booking Calendar',

                        }
                    }
                }
            }
        });
    }
    goToAnalytisc = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'AnalytiscScreen',
                options: {
                    topBar: {
                        title: {
                            text: 'Analytisc',

                        }
                    }
                }
            }
        });
    }

    goToUserProfile = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'UserProfileScreen',
                passProps: {
                    fullName: this.state.userData.fullName,
                    email: this.state.userData.email,
                    phone: this.state.userData.phone
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Host Profile',

                        }
                    }
                }
            }
        });
    }

    goToNotification = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'NotificationScreen',
                options: {
                    topBar: {
                        title: {
                            text: 'Notifications',

                        }
                    }
                }
            }
        });
    }

    async componentDidMount() {
        await NotificationService.getAll().then(response => {
            this.setState({
                totalNotificationUnRead: response.data.data != null ? response.data.data.filter(value => value.read === false).length : null
            });
        });

        await UserService.getMe().then(response => {
            console.log('userme: ', response.data);
            this.setState({
                userData: {
                    fullName: response.data.fullName,
                    email: response.data.email,
                    phone: response.data.phone
                }
            })
        })
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                {/* <Text>Xin chao</Text> */}

                {/* top view */}
                <View style={styles.viewTop}>
                    {/* <Image style={styles.imageHostBackground} source={{ uri: 'https://baoxaydung.com.vn/stores/news_dataimages/vananh/042020/06/14/in_article/4341_image001.jpg' }} /> */}
                    <Image style={styles.imgHostBackground} source={{ uri: 'https://pix10.agoda.net/hotelImages/638/6383491/6383491_18122313520070677621.jpg' }} />
                    <View style={styles.viewGreeting}>

                        <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={styles.textHostName}>Xin chao, </Text>
                                <Text style={styles.textHostEmail}>
                                    {this.state.userData.fullName}
                                </Text>
                            </View>
                            <TouchableOpacity style={{ alignSelf: 'stretch' }} onPress={this.goToNotification}>
                                <View style={{ backgroundColor: '#09BEFA5C', padding: 2.5, borderRadius: 50 }}>
                                    <Icon
                                        name="notifications"
                                        type="MaterialIcons"
                                        color="#fff"
                                    />
                                    <Text style={{
                                        position: 'absolute', color: 'white', fontWeight: 'bold', right: 0,
                                    }}>
                                        {this.state.totalNotificationUnRead < 1 ? null : this.state.totalNotificationUnRead}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.viewHostStatus}>
                            <View>
                                <Text style={styles.textHostStatus}>Host Status</Text>
                                <Text style={styles.textStatus}>Ready for Host</Text>
                            </View>
                            <View>
                                <TouchableOpacity onPress={this.goToUserProfile}>
                                    <Icon
                                        name="user"
                                        type="font-awesome"
                                        color="#fff"
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>

                {/* body view */}
                <View style={styles.viewBody}>
                    {/* row menu */}
                    <View style={styles.viewRowMenu}>
                        {/* item menu */}

                        <View style={styles.viewItem}>
                            <TouchableOpacity style={styles.touchItem} onPress={this.goToListingProperties}>
                                <View style={styles.viewTouchSub}>
                                    <Image style={styles.imgIconItem} source={require('../assets/images/icons/list-room-ic.png')} />
                                    <Text>Listing Properties</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.viewItem}>
                            <TouchableOpacity style={styles.touchItem} onPress={this.goToBookingManagement}>
                                <View style={styles.viewTouchSub}>
                                    <Image style={styles.imgIconItem} source={require('../assets/images/icons/list-booking-ic.png')} />
                                    <Text>Manage Bookings</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                    </View>
                    {/* row menu */}
                    <View style={styles.viewRowMenu}>
                        {/* item menu */}
                        <View style={styles.viewItem}>
                            <TouchableOpacity style={styles.touchItem} onPress={this.goToBookingCalendar}>
                                <View style={styles.viewTouchSub}>
                                    <Image style={styles.imgIconItem} source={require('../assets/images/icons/calendar-ic.png')} />
                                    <Text>Booking Calendar</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewItem}>
                            <TouchableOpacity style={styles.touchItem} onPress={this.goToAnalytisc}>
                                <View style={styles.viewTouchSub}>
                                    <Image style={styles.imgIconItem} source={require('../assets/images/icons/analysis-ic.png')} />
                                    <Text>Analytisc</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        // justifyContent: 'center'
    },
    viewTop: {
        width: "100%",
        flex: 1
    },
    imgHostBackground: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        width: "100%",
        height: "100%",
    },
    viewGreeting: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,.45)',
    },
    textHostName: {
        color: '#cccacb',
        fontSize: 24,
        fontStyle: 'normal',
        fontWeight: '100',
        // lineHeight: 14,
    },
    textHostEmail: {
        color: '#cccacb',
        fontStyle: 'normal',
        fontWeight: '500',
    },
    viewHostStatus: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: 'rgba(0,0,0,.45)',
        position: 'absolute',
        width: '100%',
        bottom: 0
    },
    textHostStatus: {
        color: '#cccacb',
        fontStyle: 'normal',
        fontWeight: '600',
    },
    textStatus: {
        lineHeight: 24,
        color: '#f5f2f4',
        fontStyle: 'normal',
        fontWeight: 'bold',
    },
    viewBody: {
        flex: 2,
    },
    viewRowMenu: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        marginHorizontal: 2.5

    },
    viewItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.75,
        borderRadius: 10,
        marginHorizontal: 2.5,
        marginVertical: 2.5,
        borderColor: '#F2F2F2',
        backgroundColor: '#ffff',

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.2 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 2,
    },
    touchItem: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    viewTouchSub: {
        flex: 1,
        // backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgIconItem: {
        width: 45,
        height: 45
    }
});