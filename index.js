/**
 * @format
 */
import { Navigation } from "react-native-navigation"
import messaging from '@react-native-firebase/messaging';
// import {AppRegistry} from 'react-native';
import App from './App';
// import App from './src/screens/TestScreen';
// import {name as appName} from './app.json';
import { registerScreen } from "./src/navigator/registers";

// AppRegistry.registerComponent(appName, () => App);
Navigation.registerComponent(`indexScr`, () => App);

messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
});

registerScreen();

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            stack: {
               
                children: [
                    {

                        component: {
                            name: "indexScr",
                            options:{
                                topBar: {
                                    visible: false
                                },
                                statusBar: {
                                    backgroundColor: '#1ab3c7',
                                    style: 'light'
                                }
                            }
                        }
                    }
                ]
            }
        }
    });
});