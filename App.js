import React from 'react';
import { View, Alert } from 'react-native';
import { Provider } from "react-redux";
import messaging from "@react-native-firebase/messaging";
import store from "./src/redux/store/store";
import InitialisingScreen from "./src/screens/InitialisingScreen";
import { Notifications } from 'react-native-notifications';

import { Navigation } from "react-native-navigation";

export default class App extends React.Component {


  componentDidMount() {

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );

      Navigation.push(this.props.componentId, {
        component: {
          name: 'NotificationScreen',
          options: {
            topBar: {
              title: {
                text: 'Notifications',

              }
            }
          }
        }
      });
      // navigation.navigate(remoteMessage.data.type);
    });

  }

  componentWillUnmount() {
    messaging().onMessage(async remoteMessage => {
      Notifications.postLocalNotification({
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
      });
    });
  }

  render() {
    return (
      <Provider store={store}>
        <InitialisingScreen />
      </Provider>
    )
  }

}